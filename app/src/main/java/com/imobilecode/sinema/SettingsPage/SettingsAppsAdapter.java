package com.imobilecode.sinema.SettingsPage;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.imobilecode.sinema.Helper.Retrofit.ServiceUrls;
import com.imobilecode.sinema.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by caner on 01-Sep-16.
 */
public class SettingsAppsAdapter extends RecyclerView.Adapter<SettingsAppsAdapter.MyViewHolder> {
    Context context;
    ArrayList<ApplicationItem> appList;

    public SettingsAppsAdapter(Context context, ArrayList<ApplicationItem> appList) {
        this.context = context;
        this.appList = appList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.settings_apps_custom_screen, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final ApplicationItem myPosition = appList.get(position);
        holder.settings_app_name.setText(myPosition.ApplicationName);
        Picasso.with(holder.itemView.getContext()).load(myPosition.IconPath)
                .fit().into(holder.settings_app_poster);

        if(myPosition.isInstalled){
            holder.appButton.setText(R.string.open);
        }else{
            holder.appButton.setText(R.string.download);
        }
        holder.parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openSelectedApp(myPosition);
            }
        });
        holder.appButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openSelectedApp(myPosition);
            }
        });
    }

    private void openSelectedApp(ApplicationItem selectedApplicationItem) {
        if (selectedApplicationItem.isInstalled) {
            try {
                Intent i = context.getPackageManager().getLaunchIntentForPackage(selectedApplicationItem.PackageName);
                i.addCategory(Intent.CATEGORY_LAUNCHER);
                context.startActivity(i);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            if (selectedApplicationItem.StoreLink != null && selectedApplicationItem.StoreLink.length() > 0) {
                try {
                   context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(selectedApplicationItem.StoreLink)));
                } catch (android.content.ActivityNotFoundException a) {
                    context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(ServiceUrls.iMCPlayStoreURL)));
                }
            }
        }
    }

    @Override
    public int getItemCount() {
        return appList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView settings_app_poster;
        TextView settings_app_name;
        Button appButton;
        View parent;

        public MyViewHolder(View itemView) {
            super(itemView);
            parent = itemView;
            settings_app_poster = (ImageView) itemView.findViewById(R.id.settings_apps_poster);
            settings_app_name = (TextView) itemView.findViewById(R.id.settings_apps_name);
            appButton = (Button) itemView.findViewById(R.id.app_button);
        }
    }
}
