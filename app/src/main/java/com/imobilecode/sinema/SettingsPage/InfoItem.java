package com.imobilecode.sinema.SettingsPage;

import com.imobilecode.sinema.SettingsPage.ApplicationItem;

import java.util.ArrayList;

/**
 * Created by caner on 01-Sep-16.
 */
public class InfoItem {
    public String AppStoreId;
    public ArrayList<ApplicationItem> ApplicationList;
    public String ApplicationName;
    public String IconPath;
    public String LongDescription;
    public String ShortDescription;
    public String StoreLink;
    public String URLSchema;
}