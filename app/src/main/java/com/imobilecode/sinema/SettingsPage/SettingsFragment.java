package com.imobilecode.sinema.SettingsPage;


import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.imobilecode.sinema.BuildConfig;
import com.imobilecode.sinema.Helper.MoviePreferences;
import com.imobilecode.sinema.Helper.Retrofit.RestManager;
import com.imobilecode.sinema.MovieApplication;
import com.imobilecode.sinema.R;

import org.zakariya.stickyheaders.StickyHeaderLayoutManager;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by caner on 10-Aug-16.
 */
public class SettingsFragment extends Fragment implements View.OnClickListener {
    RelativeLayout reklamli_rl, reklamsiz_rl,
            tur_language_rl, eng_language_rl;
    ImageView reklamli_iv, reklamsiz_iv, turkish_check_icon, english_check_icon;
    RestManager mManager;
    RecyclerView apps_recyclerview;
    SettingsAppsAdapter adapter;
    ArrayList<ApplicationItem> appList;
    private static final int TIME_INTERVAL = 1000;
    private static SettingsFragment mSettingsFragment;
    private long mDialogOpen;
    private long mEmailOpen;
    InfoItem appInfos;
    TextView app_version, email_text;


    public static SettingsFragment  newInstance(int instance) {

        if (mSettingsFragment == null) {
            mSettingsFragment = new SettingsFragment();
            Log.v("tag","lan " + "null");
            return mSettingsFragment;
        }
        Log.v("tag","lan " + "null degil");
        return mSettingsFragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.settings_screen, container, false);

        setHasOptionsMenu(true);

        Toolbar toolbar = (Toolbar) rootView.findViewById(R.id.settings_toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);

        apps_recyclerview = (RecyclerView) rootView.findViewById(R.id.applications_recyclerview);
        apps_recyclerview.setLayoutManager(new LinearLayoutManager(MovieApplication.getApplication()));

        reklamsiz_rl = (RelativeLayout) rootView.findViewById(R.id.reklamsiz_relative_layout);
        reklamli_rl = (RelativeLayout) rootView.findViewById(R.id.reklamli_relative_layout);
        reklamli_iv = (ImageView) rootView.findViewById(R.id.reklamli_icon);
        reklamsiz_iv = (ImageView) rootView.findViewById(R.id.reklamsiz_icon);
        tur_language_rl = (RelativeLayout) rootView.findViewById(R.id.tur_language_rl);
        eng_language_rl = (RelativeLayout) rootView.findViewById(R.id.eng_language_rl);

        turkish_check_icon = (ImageView) rootView.findViewById(R.id.turkish_check_icon);
        english_check_icon = (ImageView) rootView.findViewById(R.id.english_check_icon);

        app_version = (TextView) rootView.findViewById(R.id.app_version);
        app_version.setText("v " + BuildConfig.VERSION_NAME);

        email_text = (TextView) rootView.findViewById(R.id.email_text);

        reklamli_rl.setOnClickListener(this);
        reklamsiz_rl.setOnClickListener(this);
        tur_language_rl.setOnClickListener(this);
        eng_language_rl.setOnClickListener(this);
        email_text.setOnClickListener(this);

        if (MoviePreferences.isLanguageTurkish(MovieApplication.getApplication())) {
            turkish_check_icon.setVisibility(View.VISIBLE);
            if (english_check_icon.getVisibility() == View.VISIBLE) {
                english_check_icon.setVisibility(View.GONE);
            }
        } else {
            if (turkish_check_icon.getVisibility() == View.VISIBLE) {
                turkish_check_icon.setVisibility(View.GONE);
                english_check_icon.setVisibility(View.VISIBLE);
            }
        }

        getApplications();

        return rootView;
    }

    private void getApplications() {
        appList = new ArrayList<>();
        mManager = new RestManager();
        Call<InfoItem> listCall = mManager.getMovieService().getApplications();
        listCall.enqueue(new Callback<InfoItem>() {
            @Override
            public void onResponse(Call<InfoItem> call, Response<InfoItem> response) {
                appInfos = response.body();

                //TODO package name null geldiğinden elimle girdim
                appInfos.ApplicationList.get(0).PackageName = "com.imobilecode.eczane";

                for(int i = 0 ; i < appInfos.ApplicationList.size();i++){
                    PackageManager pm = MovieApplication.getApplication().getPackageManager();
                    try {
                        pm.getPackageInfo(appInfos.ApplicationList.get(i).PackageName, PackageManager.GET_ACTIVITIES);
                        appInfos.ApplicationList.get(i).isInstalled = true;
                    } catch (PackageManager.NameNotFoundException e) {
                        appInfos.ApplicationList.get(i).isInstalled = false;
                    }
                }

                adapter = new SettingsAppsAdapter(MovieApplication.getApplication(),appInfos.ApplicationList);
                apps_recyclerview.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<InfoItem> call, Throwable t) {

            }
        });
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.reklamsiz_relative_layout:
                if (reklamsiz_iv.getVisibility() == View.GONE) {
                    reklamsiz_iv.setVisibility(View.VISIBLE);
                    reklamli_iv.setVisibility(View.GONE);
                }
                break;
            case R.id.reklamli_relative_layout:
                if (reklamli_iv.getVisibility() == View.GONE) {
                    reklamli_iv.setVisibility(View.VISIBLE);
                    reklamsiz_iv.setVisibility(View.GONE);
                }
                break;
            case R.id.tur_language_rl:
                if (turkish_check_icon.getVisibility() == View.GONE) {
                    turkish_check_icon.setVisibility(View.VISIBLE);
                    english_check_icon.setVisibility(View.GONE);
                    MoviePreferences.setLanguageTurkish(MovieApplication.getApplication(), true);
                }
                break;
            case R.id.eng_language_rl:
                if (english_check_icon.getVisibility() == View.GONE) {
                    english_check_icon.setVisibility(View.VISIBLE);
                    turkish_check_icon.setVisibility(View.GONE);
                    MoviePreferences.setLanguageTurkish(MovieApplication.getApplication(), false);
                }
                break;
            case R.id.email_text:
                sendSupportMail();
        }
    }

    private void shareApp() {
        if (mDialogOpen + TIME_INTERVAL < System.currentTimeMillis()) {
            if (appInfos != null) {
                Intent shareIntent = new Intent();
                shareIntent.setAction(Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                shareIntent.putExtra(Intent.EXTRA_TEXT, appInfos.ShortDescription + " \n " + appInfos.StoreLink);
                PackageManager packageManager = MovieApplication.getApplication().getPackageManager();
                PackageInfo packageInfo;
                try {
                    packageInfo = packageManager.getPackageInfo(MovieApplication.getApplication().getPackageName(), 0);
                    shareIntent.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.app_name_for_share) + " (v " + packageInfo.versionName + ")");
                } catch (Exception e) {
                }
                startActivity(Intent.createChooser(shareIntent, getString(R.string.share)));
            }
        }
        mDialogOpen = System.currentTimeMillis();
    }

    private void sendSupportMail() {
        if (mEmailOpen + TIME_INTERVAL < System.currentTimeMillis()) {

            Intent intent = new Intent(Intent.ACTION_SENDTO);
            intent.setType("text/plain");
            PackageManager packageManager = MovieApplication.getApplication().getPackageManager();
            PackageInfo packageInfo;
            try {
                packageInfo = packageManager.getPackageInfo(MovieApplication.getApplication().getPackageName(), 0);
                intent.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.app_name_for_share) + " (v" + packageInfo.versionName + ")");
            } catch (Exception e) {
            }
            intent.setData(Uri.parse("mailto:support@imobilecode.com"));
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            try {
                startActivity(Intent.createChooser(intent, "Send email using..."));
            } catch (android.content.ActivityNotFoundException ex) {
                Toast.makeText(MovieApplication.getApplication(), "No email clients installed.", Toast.LENGTH_SHORT).show();
            }
        }
        mEmailOpen = System.currentTimeMillis();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.setting_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_share:
                shareApp();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
