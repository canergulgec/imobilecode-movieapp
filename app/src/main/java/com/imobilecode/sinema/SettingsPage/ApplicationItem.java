package com.imobilecode.sinema.SettingsPage;

/**
 * Created by caner on 01-Sep-16.
 */
public class ApplicationItem {
    public String AppStoreId;
    public String ApplicationName;
    public String CurentVersion;
    public String IconName;
    public String IconPath;
    public Number Id;
    public String LongDescription;
    public Number PlatformId;
    public Number Priority;
    public String ShortDescription;
    public String StoreLink;
    public String URLSchema;
    public boolean Visible;
    public boolean isInstalled;
    public String PackageName;
}
