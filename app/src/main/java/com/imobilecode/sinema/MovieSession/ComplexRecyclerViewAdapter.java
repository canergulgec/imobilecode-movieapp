package com.imobilecode.sinema.MovieSession;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.imobilecode.sinema.R;

import org.zakariya.stickyheaders.SectioningAdapter;

import java.util.List;


/**
 * Created by caner on 09-Aug-16.
 */
public class ComplexRecyclerViewAdapter extends SectioningAdapter {
    Context context;
    static final int FAVOURITE_CINEMA_HEADER_TYPE = 0;
    static final int NEAR_CINEMA_HEADER_TYPE = 1;
    static final int CITY_HEADER_TYPE = 2;
    static final int FAVOURITE_CINEMA_ITEM_TYPE = 0;
    static final int NEAR_CINEMA_ITEM_TYPE = 1;
    static final int CITY_ITEM_TYPE = 2;

    // Provide a suitable constructor (depends on the kind of dataset)
    public ComplexRecyclerViewAdapter(Context context) {
        this.context = context;
    }

    public class FavouriteCinemasItemHolder extends SectioningAdapter.ItemViewHolder {
        TextView near_movie_text;


        public FavouriteCinemasItemHolder(View itemView) {
            super(itemView);
            // near_movie_text = (TextView) itemView.findViewById(R.id.search_theatre_text);
        }
    }

    public class NearCinemasItemHolder extends SectioningAdapter.ItemViewHolder {
        TextView province_text;


        public NearCinemasItemHolder(View itemView) {
            super(itemView);
            //  province_text = (TextView) itemView.findViewById(R.id.search_theatre_text);
        }
    }

    public class CityItemHolder extends SectioningAdapter.ItemViewHolder {
        TextView province_text;


        public CityItemHolder(View itemView) {
            super(itemView);
            //  province_text = (TextView) itemView.findViewById(R.id.search_theatre_text);
        }
    }

    public class HeaderViewHolder extends SectioningAdapter.HeaderViewHolder {
        TextView textView;

        public HeaderViewHolder(View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.theatre_header_text);
        }
    }

    @Override
    public int getNumberOfSections() {
        return 3;
    }

    @Override
    public int getNumberOfItemsInSection(int sectionIndex) {
        if (sectionIndex == 0) {
            return 3;
        } else if (sectionIndex == 1) {
            return 4;
        } else {
            return 15;
        }
    }

    @Override
    public boolean doesSectionHaveHeader(int sectionIndex) {
        return true;
    }

    @Override
    public int getSectionHeaderUserType(int sectionIndex) {
        if (sectionIndex == 0) {
            return FAVOURITE_CINEMA_HEADER_TYPE;
        } else if (sectionIndex == 1) {
            return NEAR_CINEMA_HEADER_TYPE;
        } else {
            return CITY_HEADER_TYPE;
        }
    }

    @Override
    public int getSectionItemUserType(int sectionIndex, int itemIndex) {
        if (sectionIndex == 0) {
            return FAVOURITE_CINEMA_ITEM_TYPE;
        } else if (sectionIndex == 1) {
            return NEAR_CINEMA_ITEM_TYPE;
        } else {
            return CITY_ITEM_TYPE;
        }
    }

    @Override
    public ItemViewHolder onCreateItemViewHolder(ViewGroup parent, int itemType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (itemType) {
            case FAVOURITE_CINEMA_ITEM_TYPE:
                return new FavouriteCinemasItemHolder(inflater.inflate(R.layout.favourite_cinemas, parent, false));

            case NEAR_CINEMA_ITEM_TYPE:
                return new NearCinemasItemHolder(inflater.inflate(R.layout.binder_data_2, parent, false));


            case CITY_ITEM_TYPE:
                return new CityItemHolder(inflater.inflate(R.layout.search_theatre_city_custom_screen, parent, false));
        }

        throw new IllegalArgumentException("Unrecognized itemType: " + itemType);
    }

    @Override
    public HeaderViewHolder onCreateHeaderViewHolder(ViewGroup parent, int headerType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (headerType) {
            case FAVOURITE_CINEMA_HEADER_TYPE:
                return new HeaderViewHolder(inflater.inflate(R.layout.search_header, parent, false));

            case NEAR_CINEMA_HEADER_TYPE:
                return new HeaderViewHolder(inflater.inflate(R.layout.search_header, parent, false));

            case CITY_HEADER_TYPE:
                return new HeaderViewHolder(inflater.inflate(R.layout.search_header, parent, false));
        }

        throw new IllegalArgumentException("Unrecognized headerType: " + headerType);
    }

    @Override
    public void onBindItemViewHolder(SectioningAdapter.ItemViewHolder viewHolder, int sectionIndex, final int itemIndex, int itemType) {
        switch (itemType) {
            case FAVOURITE_CINEMA_ITEM_TYPE: {
                final FavouriteCinemasItemHolder ivh = (FavouriteCinemasItemHolder) viewHolder;
            }
            break;

            case NEAR_CINEMA_ITEM_TYPE: {
                NearCinemasItemHolder ivh = (NearCinemasItemHolder) viewHolder;
                break;
            }

            case CITY_ITEM_TYPE: {
                CityItemHolder ivh = (CityItemHolder) viewHolder;
                break;
            }

            default:
                throw new IllegalArgumentException("Unrecognized itemType: " + itemType);
        }
    }

    @Override
    public void onBindHeaderViewHolder(SectioningAdapter.HeaderViewHolder viewHolder, int sectionIndex, int headerType) {
        switch (headerType) {
            case FAVOURITE_CINEMA_HEADER_TYPE: {
                HeaderViewHolder hvh = (HeaderViewHolder) viewHolder;
                hvh.textView.setText("Favori Sinemalarım");
                break;
            }
            case NEAR_CINEMA_HEADER_TYPE: {
                HeaderViewHolder hvh = (HeaderViewHolder) viewHolder;
                hvh.textView.setText("Bana En Yakın Sinemalar");
                break;
            }
            case CITY_HEADER_TYPE: {
                HeaderViewHolder hvh = (HeaderViewHolder) viewHolder;
                hvh.textView.setText("İLLER");
                break;
            }

            default:
                throw new IllegalArgumentException("Unrecognized headerType: " + headerType);
        }
    }

}
