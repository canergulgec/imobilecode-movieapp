package com.imobilecode.sinema.MovieSession;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.imobilecode.sinema.Helper.MoviePreferences;
import com.imobilecode.sinema.MovieApplication;
import com.imobilecode.sinema.R;


import org.zakariya.stickyheaders.StickyHeaderLayoutManager;

import java.util.ArrayList;


/**
 * Created by caner on 05-Aug-16.
 */
public class MovieSessionFragment extends Fragment {
    RecyclerView monday_session_recyclerview;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.monday_session, container, false);

        MoviePreferences.setViewholder2FirstOpened(MovieApplication.getApplication(), true);

        monday_session_recyclerview = (RecyclerView) rootView.findViewById(R.id.monday_session_recyclerview);
        bindDataToAdapter();

        return rootView;
    }

    private void bindDataToAdapter() {
        // Bind adapter to recycler view object

        ComplexRecyclerViewAdapter adapter = new ComplexRecyclerViewAdapter(getContext());
        monday_session_recyclerview.setLayoutManager(new StickyHeaderLayoutManager());
        monday_session_recyclerview.setAdapter(adapter);
    }
}
