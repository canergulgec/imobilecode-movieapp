package com.imobilecode.sinema.DetailMoviePage;


import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.VideoView;

import com.imobilecode.sinema.CustomTextView.CustomFontTextView;
import com.imobilecode.sinema.CustomTextView.FontEnum;
import com.imobilecode.sinema.CustomTextView.TypefaceSpan;
import com.imobilecode.sinema.Helper.MoviePreferences;
import com.imobilecode.sinema.Helper.Retrofit.RestManager;
import com.imobilecode.sinema.MovieSession.SessionActivity;
import com.imobilecode.sinema.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by caner on 14-Jul-16.
 */
public class DetailedMovieActivity extends AppCompatActivity implements View.OnClickListener {
    String video_path;
    ImageView movie_prof;
    TextView movie_first_name, movie_genre,
            movie_duration, movie_director, movie_description,
            movie_second_name;
    CustomFontTextView movie_cast;
    RestManager mManager;
    LinearLayout linearLayout, seanslar_button;
    ProgressBar progressBar;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detailed_movie);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        MoviePreferences.setDetailActivityActive(getApplicationContext(), true);


        linearLayout = (LinearLayout) findViewById(R.id.detail_linearLayout);
        seanslar_button = (LinearLayout) findViewById(R.id.button_linear_layout);
        progressBar = (ProgressBar) findViewById(R.id.progressbar_detail);

        movie_prof = (ImageView) findViewById(R.id.movie_profile);
        movie_first_name = (TextView) findViewById(R.id.movie_detailed_first_name);
        movie_second_name = (TextView) findViewById(R.id.movie_detailed_second_name);
        movie_genre = (TextView) findViewById(R.id.movie_genre_tv);
        movie_cast = (CustomFontTextView) findViewById(R.id.movie_cast_tv);
        movie_duration = (TextView) findViewById(R.id.movie_duration_tv);
        movie_director = (TextView) findViewById(R.id.movie_director_tv);
        movie_description = (TextView) findViewById(R.id.movie_description_tv);

        seanslar_button.setOnClickListener(this);

        intent = getIntent();

        if (getIntent().hasExtra("byteArray")) {
            Bitmap b = BitmapFactory.decodeByteArray(
                    getIntent().getByteArrayExtra("byteArray"), 0, getIntent().getByteArrayExtra("byteArray").length);
            movie_prof.setImageBitmap(b);
        }

        movieAttributes();

        final VideoView videoView =
                (VideoView) findViewById(R.id.videoview);

        videoView.setVideoPath(
                "https://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4");
        video_path = "https://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4";

      //  videoView.start();

        videoView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(Uri.parse(video_path), "video/*");
                startActivity(intent);

                return false;
            }
        });

    }

    private void movieAttributes() {
        final String movie_id = intent.getStringExtra("movie_id");

        mManager = new RestManager();
        Call<MovieDetailResultItem> listCall = mManager.getMovieService().getDetailedMovie(movie_id);
        listCall.enqueue(new Callback<MovieDetailResultItem>() {
            @Override
            public void onResponse(Call<MovieDetailResultItem> call, Response<MovieDetailResultItem> response) {
                if (response.isSuccessful()) {
                    MovieDetailResultItem detailedMovie = response.body();

                    if (MoviePreferences.isLanguageTurkish(getApplicationContext())) {
                        movie_first_name.setText(detailedMovie.MovieDetailItem.TrName);
                        if (detailedMovie.MovieDetailItem.EnName != null) {
                            movie_second_name.setVisibility(View.VISIBLE);
                            movie_second_name.setText(detailedMovie.MovieDetailItem.EnName);
                        }
                    } else {
                        if (detailedMovie.MovieDetailItem.EnName != null) {
                            movie_first_name.setText(detailedMovie.MovieDetailItem.EnName);
                            movie_second_name.setVisibility(View.VISIBLE);
                            movie_second_name.setText(detailedMovie.MovieDetailItem.TrName);
                        } else {
                            movie_first_name.setText(detailedMovie.MovieDetailItem.TrName);
                            movie_second_name.setVisibility(View.GONE);
                        }
                    }
                    movie_genre.setText(" " + detailedMovie.MovieDetailItem.Genre);
                    movie_duration.setText(" " + detailedMovie.MovieDetailItem.Duration + " dk.");
                    movie_director.setText(" " + detailedMovie.MovieDetailItem.Director);
                    movie_description.setText(detailedMovie.MovieDetailItem.Description);

                    setCustomFontTextView(detailedMovie.MovieDetailItem.Cast);

                    linearLayout.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<MovieDetailResultItem> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
            }
        });

    }

    private void setCustomFontTextView(String cast) {
        SpannableString spannableString = new SpannableString("Oyuncular: " + cast);

        spannableString.setSpan(new TypefaceSpan(FontEnum.ROBOTO_REGULAR),
                0, 10,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        if (cast != null) {
            spannableString.setSpan(new TypefaceSpan(FontEnum.ROBOTO_LIGHT),
                    11, 11 + cast.length()
                    , Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        }

        movie_cast.setText(spannableString);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.button_linear_layout:
                startActivity(new Intent(this, SessionActivity.class));
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}


