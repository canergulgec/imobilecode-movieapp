package com.imobilecode.sinema.DetailMoviePage;

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Created by caner on 29-Jul-16.
 */
public class MovieDetailItem {
    public String Id;
    public String TrName;
    public String EnName;
    public String TrPosterUrl;
    public String EnPosterUrl;
    //// FIXME: 29-Jul-16 traılrt acmayı unutma
   // public ArrayList<String> Trailer;
    public String Director;
    public String Cast;
    public int Duration;
    public String Genre;
    public int Year;
    public String Description;
    public double ImdbRate;
    public double RottenRate;
    public boolean IsPlaying;
}
