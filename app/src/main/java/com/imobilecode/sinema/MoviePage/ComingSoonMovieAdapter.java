package com.imobilecode.sinema.MoviePage;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.Image;
import android.os.Build;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.imobilecode.sinema.DetailMoviePage.DetailedMovieActivity;
import com.imobilecode.sinema.R;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by caner on 24-Aug-16.
 */
public class ComingSoonMovieAdapter extends RecyclerView.Adapter<ComingSoonMovieAdapter.MyViewHolder> {
    ArrayList<MovieListItem> movieList;
    Context context;
    String currentDate;
    String result;

    public ComingSoonMovieAdapter(Context context, ArrayList<MovieListItem> movieList, String currentDate) {
        this.movieList = movieList;
        this.context = context;
        this.currentDate = currentDate;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.coming_soon_movie_custom_view, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final MovieListItem current = movieList.get(position);
        String movieReleaseDate = current.ReleaseDate.substring(0, 10);

        String differenceOfDates = getDate(movieReleaseDate);
        holder.movie_release.setText(differenceOfDates + " gün kaldı");

        Picasso.with(holder.itemView.getContext()).load(current.TrPosterUrl)
                .fit().into(holder.movie_profile, new com.squareup.picasso.Callback() {
            @Override
            public void onSuccess() {

                holder.progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onError() {
            }
        });

        holder.movie_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    ActivityOptionsCompat optionsCompat = null;
                    optionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation((Activity) context, v, v.getTransitionName());
                    Intent intent = new Intent(context, DetailedMovieActivity.class);
                    intent.putExtra("movie_id", current.Id);

                    v.buildDrawingCache();
                    Bitmap image = v.getDrawingCache();
                    ByteArrayOutputStream bs = new ByteArrayOutputStream();
                    image.compress(Bitmap.CompressFormat.JPEG, 99, bs);
                    intent.putExtra("byteArray", bs.toByteArray());
                    context.startActivity(intent, optionsCompat.toBundle());
                }
            }
        });
    }

    private String getDate(String movieReleaseDate) {
        String dayDifference = null;
        try {
            Date date1;
            Date date2;

            SimpleDateFormat dates = new SimpleDateFormat("yyyy-MM-dd");

            //Setting dates
            date1 = dates.parse(movieReleaseDate);
            date2 = dates.parse(currentDate);

            //Comparing dates
            long difference = Math.abs(date1.getTime() - date2.getTime());
            long differenceDates = difference / (24 * 60 * 60 * 1000);

            //Convert long to String
            dayDifference = Long.toString(differenceDates);
        } catch (Exception exception) {
        }
        return dayDifference;
    }

    @Override
    public int getItemCount() {
        return movieList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView movie_profile;
        TextView movie_release;
        ProgressBar progressBar;

        public MyViewHolder(View itemView) {
            super(itemView);
            movie_profile = (ImageView) itemView.findViewById(R.id.coming_movie_pic);
            movie_release = (TextView) itemView.findViewById(R.id.coming_movie_release_date);
            progressBar = (ProgressBar) itemView.findViewById(R.id.coming_soon_progressbar);
        }
    }
}
