package com.imobilecode.sinema.MoviePage;


import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.transition.TransitionInflater;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.imobilecode.sinema.Helper.Retrofit.RestManager;
import com.imobilecode.sinema.MovieApplication;
import com.imobilecode.sinema.R;

import java.util.Collections;
import java.util.Comparator;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PlayingMoviesFragment extends Fragment {
    RecyclerView myRecycleview;
    PlayingMovieAdapter adapter;
    RestManager mManager;
    Call<MovieResultItem> listCall;
    ProgressBar progressBar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.playing_movie_recyclerview, container, false);

        if (Build.VERSION.SDK_INT >= 21) {
            getActivity().getWindow().setSharedElementEnterTransition(TransitionInflater.from(MovieApplication.getApplication()).inflateTransition(R.transition.shared_element_transition));
        }
        myRecycleview = (RecyclerView) rootView.findViewById(R.id.playing_movie_recyclerview);
        progressBar = (ProgressBar) rootView.findViewById(R.id.playing_movie_progressbar);

        getMovieData();

        return rootView;
    }

    private void getMovieData() {
        mManager = new RestManager();
        listCall = mManager.getMovieService().getAllMovies(2);
        listCall.enqueue(new Callback<MovieResultItem>() {
            @Override
            public void onResponse(Call<MovieResultItem> call, Response<MovieResultItem> response) {
                if (response.isSuccessful()) {
                    MovieResultItem movieList = response.body();
                    progressBar.setVisibility(View.GONE);
                    if (movieList.MovieList != null) {
                        final GridLayoutManager manager = new GridLayoutManager(getActivity(), 2);

                        //Todo imdb ratelerini kendi kafama göre verdim..
                        movieList.MovieList.get(1).ImdbRate = 0.1;
                        movieList.MovieList.get(2).ImdbRate = 4.1;
                        movieList.MovieList.get(3).ImdbRate = 2.0;
                        movieList.MovieList.get(4).ImdbRate = 2.5;
                        movieList.MovieList.get(5).ImdbRate = 3.8;

                        movieList.MovieList.get(1).RottenRate = 18.2;
                        movieList.MovieList.get(2).RottenRate = 16.6;
                        movieList.MovieList.get(3).RottenRate = 65.7;
                        movieList.MovieList.get(4).RottenRate = 32.5;
                        movieList.MovieList.get(5).RottenRate = 17.5;
                        /////////////////

                        for (int i = 0; i < movieList.MovieList.size(); i++) {
                            movieList.MovieList.get(i).ReleaseDate = movieList.MovieList.get(i).ReleaseDate.substring(0, 10);
                        }
                        Collections.sort(movieList.MovieList, Collections.reverseOrder(new ReleaseDateComparator()));

                        myRecycleview.setLayoutManager(manager);
                        myRecycleview.setHasFixedSize(true);

                        adapter = new PlayingMovieAdapter(getActivity(), movieList.MovieList);
                        myRecycleview.setAdapter(adapter);

                        manager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                            @Override
                            public int getSpanSize(int position) {
                                return adapter.isPositionHeader(position) ? manager.getSpanCount() : 1;
                            }
                        });
                    }
                }
            }

            @Override
            public void onFailure(Call<MovieResultItem > call, Throwable t) {
                if (call.isCanceled()) {
                    Log.v("this", "request " + "canceled");
                } else {
                    Log.v("this", "other" + "asd");
                }
            }
        });
    }

    public class ReleaseDateComparator implements Comparator<MovieListItem> {
        @Override
        public int compare(MovieListItem obj1, MovieListItem obj2) {
            return obj1.ReleaseDate.compareTo(obj2.ReleaseDate);
        }
    }

    @Override
    public void onDestroy() {
        listCall.cancel();
        super.onDestroy();
    }

}
