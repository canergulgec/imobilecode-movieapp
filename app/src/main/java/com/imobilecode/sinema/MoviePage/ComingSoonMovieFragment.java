package com.imobilecode.sinema.MoviePage;


import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.transition.TransitionInflater;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.imobilecode.sinema.Helper.Retrofit.RestManager;
import com.imobilecode.sinema.MovieApplication;
import com.imobilecode.sinema.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by caner on 24-Aug-16.
 */
public class ComingSoonMovieFragment extends Fragment {
    RecyclerView myRecycleview;
    RestManager mManager;
    Call<MovieResultItem> listCall;
    ComingSoonMovieAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.coming_soon_movie_recyclerview, container, false);

        if (Build.VERSION.SDK_INT >= 21) {
            getActivity().getWindow().setSharedElementEnterTransition(TransitionInflater.from(MovieApplication.getApplication()).inflateTransition(R.transition.shared_element_transition));
        }

        myRecycleview = (RecyclerView) rootView.findViewById(R.id.coming_soon_recyclerview);

        getMovieData();
        return rootView;
    }

    private void getMovieData() {
        mManager = new RestManager();
        listCall = mManager.getMovieService().getAllMovies(0);
        listCall.enqueue(new Callback<MovieResultItem>() {
            @Override
            public void onResponse(Call<MovieResultItem> call, Response<MovieResultItem> response) {
                if (response.isSuccessful()) {
                    MovieResultItem movieList = response.body();
                    if (movieList.MovieList != null) {
                        for (int i = 0; i < movieList.MovieList.size(); i++) {
                            movieList.MovieList.get(i).ReleaseDate = movieList.MovieList.get(i).ReleaseDate.substring(0, 10);
                        }
                        Collections.sort(movieList.MovieList,new ReleaseDateSort());

                        final GridLayoutManager manager = new GridLayoutManager(getActivity(), 2);
                        myRecycleview.setLayoutManager(manager);
                        myRecycleview.setHasFixedSize(true);

                        Date date = Calendar.getInstance().getTime();
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                        String currentDate = sdf.format(date);

                        adapter = new ComingSoonMovieAdapter(getActivity(), movieList.MovieList, currentDate);
                        myRecycleview.setAdapter(adapter);
                    }

                }
            }

            @Override
            public void onFailure(Call<MovieResultItem> call, Throwable t) {
                if (call.isCanceled()) {
                    Log.v("this", "request " + "canceled");
                } else {
                }

            }
        });
    }

    public class ReleaseDateSort implements Comparator<MovieListItem> {
        @Override
        public int compare(MovieListItem obj1, MovieListItem obj2) {
            return obj1.ReleaseDate.compareTo(obj2.ReleaseDate);
        }
    }

    @Override
    public void onDestroy() {
        listCall.cancel();
        super.onDestroy();
    }
}
