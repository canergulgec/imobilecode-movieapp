package com.imobilecode.sinema.MoviePage;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.imobilecode.sinema.DetailMoviePage.DetailedMovieActivity;
import com.imobilecode.sinema.R;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by caner on 14-Jul-16.
 */
public class PlayingMovieAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    ArrayList<MovieListItem> getItems;
    Context context;
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;

    public PlayingMovieAdapter(Context context, ArrayList<MovieListItem> getItems) {
        this.context = context;
        this.getItems = getItems;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_HEADER) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_header, parent, false);
            return new VHHeader(v);
        } else if (viewType == TYPE_ITEM) {
            View view = LayoutInflater.from(context).inflate(R.layout.playing_movie_custom_view, parent, false);
            return new VHItem(view);
        }
        throw new RuntimeException("there is no type that matches the type " + viewType + " + make sure your using types correctly");
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof VHHeader) {
            final VHHeader VHHeader = (VHHeader) holder;
            VHHeader.header_linear.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (VHHeader.header_sorted_text.getText().toString().equals(context.getResources().getString(R.string.vizyon_tarihi_sort))) {
                        VHHeader.txtHeader.setText(context.getResources().getString(R.string.imdb_point_sort));
                        VHHeader.header_sorted_text.setText(context.getResources().getString(R.string.imdb_point_sort_big));
                        Collections.sort(getItems, Collections.reverseOrder(new ImdbPointComparator()));
                        notifyDataSetChanged();

                    } else if (VHHeader.header_sorted_text.getText().toString().equals(context.getResources().getString(R.string.imdb_point_sort_big))) {
                        VHHeader.txtHeader.setText(context.getResources().getString(R.string.rotten_point_sort));
                        VHHeader.header_sorted_text.setText(context.getResources().getString(R.string.rotten_point_big));
                        Collections.sort(getItems, Collections.reverseOrder(new RottenRateComparator()));
                        notifyDataSetChanged();
                    } else {
                        VHHeader.txtHeader.setText(context.getResources().getString(R.string.header_text));
                        VHHeader.header_sorted_text.setText(context.getResources().getString(R.string.vizyon_tarihi_sort));
                        Collections.sort(getItems, Collections.reverseOrder(new ReleaseComparator()));
                        notifyDataSetChanged();
                    }
                }
            });


        } else if (holder instanceof VHItem) {
            final MovieListItem current = getItems.get(position - 1);
            final VHItem VHitem = (VHItem) holder;
            VHitem.progressBar.setVisibility(View.VISIBLE);
            VHitem.imdb_point.setText("" + current.ImdbRate);
            VHitem.rotten_rate.setText("" + current.RottenRate);


            Picasso.with(holder.itemView.getContext()).load(current.TrPosterUrl)
                    .fit().into(VHitem.movie_image, new com.squareup.picasso.Callback() {
                @Override
                public void onSuccess() {
                    VHitem.progressBar.setVisibility(View.GONE);

                }

                @Override
                public void onError() {

                }
            });

            VHitem.movie_image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        ActivityOptionsCompat optionsCompat = null;
                        optionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation((Activity) context, v, v.getTransitionName());
                        Intent intent = new Intent(context, DetailedMovieActivity.class);
                        intent.putExtra("movie_id", current.Id);

                        v.buildDrawingCache();
                        Bitmap image = v.getDrawingCache();
                        ByteArrayOutputStream bs = new ByteArrayOutputStream();
                        image.compress(Bitmap.CompressFormat.JPEG, 99, bs);
                        intent.putExtra("byteArray", bs.toByteArray());

                        context.startActivity(intent, optionsCompat.toBundle());
                    }
                }
            });
        }
    }

    public int getItemViewType(int position) {
        if (isPositionHeader(position)) {
            return TYPE_HEADER;
        } else {
            return TYPE_ITEM;
        }
    }

    public boolean isPositionHeader(int position) {
        return position == 0;
    }

    @Override
    public int getItemCount() {
        return getItems.size() + 1;
    }

    class VHHeader extends RecyclerView.ViewHolder {
        TextView txtHeader;
        TextView header_sorted_text;
        LinearLayout header_linear;

        public VHHeader(View itemView) {
            super(itemView);
            this.txtHeader = (TextView) itemView.findViewById(R.id.header_vizyon);
            this.header_sorted_text = (TextView) itemView.findViewById(R.id.header_sorted_text);
            this.header_linear = (LinearLayout) itemView.findViewById(R.id.recycleview_header_linearlayout);
        }
    }

    class VHItem extends RecyclerView.ViewHolder {
        TextView rotten_rate, imdb_point;
        ImageView movie_image;
        ProgressBar progressBar;

        public VHItem(View itemView) {
            super(itemView);
            this.rotten_rate = (TextView) itemView.findViewById(R.id.movie_rotten_rate);
            this.movie_image = (ImageView) itemView.findViewById(R.id.movie_pic);
            this.progressBar = (ProgressBar) itemView.findViewById(R.id.main_screen_pb);
            this.imdb_point = (TextView) itemView.findViewById(R.id.movie_imdb_point);

        }
    }

    public class ImdbPointComparator implements Comparator<MovieListItem> {
        @Override
        public int compare(MovieListItem obj1, MovieListItem obj2) {
            return Double.compare(obj1.ImdbRate, obj2.ImdbRate);
        }
    }

    public class RottenRateComparator implements Comparator<MovieListItem> {
        @Override
        public int compare(MovieListItem obj1, MovieListItem obj2) {
            return Double.compare(obj1.RottenRate, obj2.RottenRate);
        }
    }

    public class ReleaseComparator implements Comparator<MovieListItem> {
        @Override
        public int compare(MovieListItem obj1, MovieListItem obj2) {
            return obj1.ReleaseDate.compareTo(obj2.ReleaseDate);
        }
    }
}
