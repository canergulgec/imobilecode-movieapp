package com.imobilecode.sinema.MoviePage;


import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.imobilecode.sinema.R;
import com.imobilecode.sinema.SearchPage.SearchFragment;

/**
 * Created by caner on 24-Aug-16.
 */
public class MovieFragment extends Fragment {
    ViewPager viewPager;
    private TabLayout tabLayout;
    private static MovieFragment mMovieFragment;
    View rootView;

    public static MovieFragment  newInstance(int instance) {
        if (mMovieFragment == null) {
            mMovieFragment = new MovieFragment();
            return mMovieFragment;
        }
        return mMovieFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.movie_viewpager, container, false);

        viewPager = (ViewPager) rootView.findViewById(R.id.movies_viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) rootView.findViewById(R.id.movies_tablayout);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setSelectedTabIndicatorColor(Color.WHITE);

        return rootView;
    }

    private void setupViewPager(ViewPager viewPager) {
        MovieViewPagerAdapter adapter = new MovieViewPagerAdapter(getChildFragmentManager());
        adapter.addFragment(new PlayingMoviesFragment(), "Vizyondakiler");
        adapter.addFragment(new ComingSoonMovieFragment(), "Yakındakiler");
        viewPager.setAdapter(adapter);
    }
}
