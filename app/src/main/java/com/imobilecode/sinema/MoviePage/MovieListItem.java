package com.imobilecode.sinema.MoviePage;

/**
 * Created by caner on 29-Jul-16.
 */
public class MovieListItem {
    public String Id;
    public String TrPosterUrl;
    public String EnPosterUrl;
    public double ImdbRate;
    public double RottenRate;
    public String ReleaseDate;
    public boolean IsPlaying;
}
