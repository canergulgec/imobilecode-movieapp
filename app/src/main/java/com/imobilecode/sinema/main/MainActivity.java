package com.imobilecode.sinema.main;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.Toast;

import com.imobilecode.sinema.CinemaPage.CinemaFragment;
import com.imobilecode.sinema.Helper.FragNavController;
import com.imobilecode.sinema.MoviePage.MovieFragment;
import com.imobilecode.sinema.R;
import com.imobilecode.sinema.SearchPage.SearchFragment;
import com.imobilecode.sinema.SettingsPage.SettingsFragment;
import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnTabReselectListener;
import com.roughike.bottombar.OnTabSelectListener;
import com.imobilecode.sinema.Helper.FragmentNavCallback;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity implements FragNavController.TransactionListener ,FragNavController.RootFragmentListener,FragmentNavCallback{
    private BottomBar mBottomBar;
    private FragNavController mNavController;
    private MovieFragment movieFragment;
    private SearchFragment searchFragment;
    private SettingsFragment settingsFragment;
    private CinemaFragment cinemaFragment;
    private final int INDEX_MOVIE = FragNavController.TAB1;
    private final int INDEX_CINEMA = FragNavController.TAB2;
    private final int INDEX_SEARCH = FragNavController.TAB3;
    private final int INDEX_SETTINGS = FragNavController.TAB4;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);




        movieFragment = new MovieFragment();
        cinemaFragment = new CinemaFragment();
        searchFragment = new SearchFragment();
        settingsFragment = new SettingsFragment();

        ArrayList<Fragment> fragments = new ArrayList<>(5);

        fragments.add(movieFragment);
        fragments.add(cinemaFragment);
        fragments.add(searchFragment);
        fragments.add(settingsFragment);

        mNavController = FragNavController.newBuilder(savedInstanceState, getSupportFragmentManager(), R.id.fragmentContainer)
                .transactionListener(this)
                .rootFragmentListener(this, 4)
                .build();

        BottomBar bottomBar = (BottomBar) findViewById(R.id.bottomBar);
        bottomBar.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelected(@IdRes int tabId) {
                switch (tabId) {
                    case R.id.bb_menu_movies:
                        mNavController.switchTab(INDEX_MOVIE);
                        break;
                    case R.id.bb_menu_search:
                        mNavController.switchTab(INDEX_SEARCH);
                        break;
                    case R.id.bb_menu_settings:
                        mNavController.switchTab(INDEX_SETTINGS);
                        break;

                    case R.id.bb_menu_cinema:
                        mNavController.switchTab(INDEX_CINEMA);
                        break;
                }
            }
        });

        bottomBar.setOnTabReselectListener(new OnTabReselectListener() {
            @Override
            public void onTabReSelected(@IdRes int tabId) {

                mNavController.clearStack();
            }
        });
    }


    @Override
    public void onBackPressed() {
        if (!mNavController.isRootFragment()) {
            mNavController.popFragment();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mNavController != null) {
            mNavController.onSaveInstanceState(outState);
        }
    }

    @Override
    public void pushFragment(Fragment fragment) {
        if (mNavController != null) {
            mNavController.pushFragment(fragment);
        }
    }

    @Override
    public void onTabTransaction(Fragment fragment, int index) {
        // If we have a backstack, show the back button
        if (getSupportActionBar() != null && mNavController != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(!mNavController.isRootFragment());
        }
    }

    @Override
    public void onFragmentTransaction(Fragment fragment, FragNavController.TransactionType transactionType) {
        //do fragmentty stuff. Maybe change title, I'm not going to tell you how to live your life
        // If we have a backstack, show the back button
        if (getSupportActionBar() != null && mNavController != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(!mNavController.isRootFragment());
        }
    }


    @Override
    public Fragment getRootFragment(int index) {
        switch (index) {
            case INDEX_MOVIE:
                return MovieFragment.newInstance(0);
            case INDEX_CINEMA:
                return CinemaFragment.newInstance(0);
            case INDEX_SEARCH:
                return SearchFragment.newInstance(0);
            case INDEX_SETTINGS:
                return SettingsFragment.newInstance(0);
        }
        throw new IllegalStateException("Need to send an index that we know");
    }


}

