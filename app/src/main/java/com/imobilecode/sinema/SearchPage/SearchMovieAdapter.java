package com.imobilecode.sinema.SearchPage;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.support.v4.app.ActivityOptionsCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.imobilecode.sinema.DetailMoviePage.DetailedMovieActivity;
import com.imobilecode.sinema.Helper.MoviePreferences;
import com.imobilecode.sinema.SearchPage.MovieSearchResultItem;
import com.imobilecode.sinema.CinemaSessionPage.CinemaSessionActivity;
import com.imobilecode.sinema.R;
import com.squareup.picasso.Picasso;

import org.zakariya.stickyheaders.SectioningAdapter;

import java.io.ByteArrayOutputStream;

/**
 * Created by caner on 12-Aug-16.
 */
public class SearchMovieAdapter extends SectioningAdapter {
    int playingMovies;
    int comingSoonMovies;
    int theaters;
    int cities;
    int headercount = 0;
    Context context;
    MovieSearchResultItem movieList;
    static final int PLAYING_HEADER_TYPE = 0;
    static final int COMINGSOON_HEADER_TYPE = 1;
    static final int THEATRE_HEADER_TYPE = 2;
    static final int CITY_HEADER_TYPE = 3;

    static final int PLAYING_ITEM_TYPE = 0;
    static final int COMINGSOON_ITEM_TYPE = 1;
    static final int THEATRE_ITEM_TYPE = 2;
    static final int CITY_ITEM_TYPE = 3;

    public SearchMovieAdapter(Context context, MovieSearchResultItem movieList) {
        this.context = context;
        this.movieList = movieList;

        playingMovies = movieList.PlayingMovies.size();
        comingSoonMovies = movieList.ComingSoonMovies.size();
        theaters = movieList.Theaters.size();
        cities = movieList.Cities.size();

        if (playingMovies > 0) {
            headercount++;
        }
        if (comingSoonMovies > 0) {
            headercount++;
        }
        if (theaters > 0) {
            headercount++;
        }
        if (cities > 0) {
            headercount++;
        }
    }

    public class PlayingMovieItemViewHolder extends SectioningAdapter.ItemViewHolder {
        ImageView movie_poster;
        TextView movie_name;
        ProgressBar movie_progressbar;
        RelativeLayout search_relativelayout;

        public PlayingMovieItemViewHolder(View itemView) {
            super(itemView);
            movie_poster = (ImageView) itemView.findViewById(R.id.search_movie_poster);
            movie_name = (TextView) itemView.findViewById(R.id.search_movie_name);
            movie_progressbar = (ProgressBar) itemView.findViewById(R.id.search_movie_progressbar);
            search_relativelayout = (RelativeLayout) itemView.findViewById(R.id.search_relative_layout);
        }
    }

    public class ComingSoonMovieItemViewHolder extends SectioningAdapter.ItemViewHolder {
        ImageView movie_poster;
        TextView movie_name;
        ProgressBar movie_progressbar;
        RelativeLayout search_relativelayout;

        public ComingSoonMovieItemViewHolder(View itemView) {
            super(itemView);
            movie_poster = (ImageView) itemView.findViewById(R.id.search_movie_poster);
            movie_name = (TextView) itemView.findViewById(R.id.search_movie_name);
            movie_progressbar = (ProgressBar) itemView.findViewById(R.id.search_movie_progressbar);
            search_relativelayout = (RelativeLayout) itemView.findViewById(R.id.search_relative_layout);
        }
    }


    public class TheatreItemViewHolder extends SectioningAdapter.ItemViewHolder {
        TextView theatre_name;
        View parent;

        public TheatreItemViewHolder(View itemView) {
            super(itemView);
            parent = itemView;
            theatre_name = (TextView) itemView.findViewById(R.id.search_theatre_text);
        }
    }

    public class CityItemViewHolder extends SectioningAdapter.ItemViewHolder {
        TextView city_name;
        View parent;

        public CityItemViewHolder(View itemView) {
            super(itemView);
            parent = itemView;
            city_name = (TextView) itemView.findViewById(R.id.search_theatre_text);

        }
    }

    public class HeaderViewHolder extends SectioningAdapter.HeaderViewHolder {
        TextView textView;

        public HeaderViewHolder(View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.theatre_header_text);
        }
    }

    @Override
    public int getNumberOfSections() {
        return headercount;
    }

    @Override
    public int getNumberOfItemsInSection(int sectionIndex) {
        if (headercount == 4) {
            if (sectionIndex == 0) {
                return playingMovies;
            } else if (sectionIndex == 1) {
                return comingSoonMovies;
            } else if (sectionIndex == 2) {
                return theaters;
            } else if (sectionIndex == 3) {
                return cities;
            }

        } else if (headercount == 3) {
            if (playingMovies > 0 && comingSoonMovies > 0 && theaters > 0) {
                if (sectionIndex == 0) {
                    return playingMovies;
                } else if (sectionIndex == 1) {
                    return comingSoonMovies;
                } else if (sectionIndex == 2) {
                    return theaters;
                }
            } else if (comingSoonMovies > 0 && theaters > 0 && cities > 0) {
                if (sectionIndex == 0) {
                    return comingSoonMovies;
                } else if (sectionIndex == 1) {
                    return theaters;
                } else if (sectionIndex == 2) {
                    return cities;
                }
            } else if (playingMovies > 0 && theaters > 0 && cities > 0) {
                if (sectionIndex == 0) {
                    return playingMovies;
                } else if (sectionIndex == 1) {
                    return theaters;
                } else if (sectionIndex == 2) {
                    return cities;
                }
            } else if (playingMovies > 0 && comingSoonMovies > 0 && cities > 0) {
                if (sectionIndex == 0) {
                    return playingMovies;
                } else if (sectionIndex == 1) {
                    return comingSoonMovies;
                } else if (sectionIndex == 2) {
                    return cities;
                }
            }

        } else if (headercount == 2) {
            if (playingMovies > 0 && comingSoonMovies > 0) {
                if (sectionIndex == 0) {
                    return playingMovies;
                } else if (sectionIndex == 1) {
                    return comingSoonMovies;
                }
            } else if (playingMovies > 0 && theaters > 0) {
                if (sectionIndex == 0) {
                    return playingMovies;
                } else if (sectionIndex == 1) {
                    return theaters;
                }
            } else if (playingMovies > 0 && cities > 0) {
                if (sectionIndex == 0) {
                    return playingMovies;
                } else if (sectionIndex == 1) {
                    return cities;
                }
            } else if (comingSoonMovies > 0 && theaters > 0) {
                if (sectionIndex == 0) {
                    return comingSoonMovies;
                } else if (sectionIndex == 1) {
                    return theaters;
                }
            } else if (comingSoonMovies > 0 && cities > 0) {
                if (sectionIndex == 0) {
                    return comingSoonMovies;
                } else if (sectionIndex == 1) {
                    return cities;
                }
            } else if (theaters > 0 && cities > 0) {
                if (sectionIndex == 0) {
                    return theaters;
                } else if (sectionIndex == 1) {
                    return cities;
                }
            }

        } else if (headercount == 1) {
            if (playingMovies > 0) {
                return playingMovies;
            } else if (comingSoonMovies > 0) {
                return comingSoonMovies;
            } else if (theaters > 0) {
                return theaters;
            } else if (cities > 0) {
                return cities;
            }
        }
        return 0;
    }

    @Override
    public boolean doesSectionHaveHeader(int sectionIndex) {
        return true;
    }

    @Override
    public int getSectionHeaderUserType(int sectionIndex) {
        if (headercount == 4) {
            if (sectionIndex == 0) {
                return PLAYING_HEADER_TYPE;
            } else if (sectionIndex == 1) {
                return COMINGSOON_HEADER_TYPE;
            } else if (sectionIndex == 2) {
                return THEATRE_HEADER_TYPE;
            } else if (sectionIndex == 3) {
                return CITY_HEADER_TYPE;
            }

        } else if (headercount == 3) {
            if (playingMovies > 0 && comingSoonMovies > 0 && theaters > 0) {
                if (sectionIndex == 0) {
                    return PLAYING_HEADER_TYPE;
                } else if (sectionIndex == 1) {
                    return COMINGSOON_HEADER_TYPE;
                } else if (sectionIndex == 2) {
                    return THEATRE_HEADER_TYPE;
                }
            } else if (comingSoonMovies > 0 && theaters > 0 && cities > 0) {
                if (sectionIndex == 0) {
                    return COMINGSOON_HEADER_TYPE;
                } else if (sectionIndex == 1) {
                    return THEATRE_HEADER_TYPE;
                } else if (sectionIndex == 2) {
                    return CITY_HEADER_TYPE;
                }
            } else if (playingMovies > 0 && theaters > 0 && cities > 0) {
                if (sectionIndex == 0) {
                    return PLAYING_HEADER_TYPE;
                } else if (sectionIndex == 1) {
                    return THEATRE_HEADER_TYPE;
                } else if (sectionIndex == 2) {
                    return CITY_HEADER_TYPE;
                }
            } else if (playingMovies > 0 && comingSoonMovies > 0 && cities > 0) {
                if (sectionIndex == 0) {
                    return PLAYING_HEADER_TYPE;
                } else if (sectionIndex == 1) {
                    return COMINGSOON_HEADER_TYPE;
                } else if (sectionIndex == 2) {
                    return CITY_HEADER_TYPE;
                }
            }

        } else if (headercount == 2) {
            if (playingMovies > 0 && comingSoonMovies > 0) {
                if (sectionIndex == 0) {
                    return PLAYING_HEADER_TYPE;
                } else if (sectionIndex == 1) {
                    return COMINGSOON_HEADER_TYPE;
                }
            } else if (playingMovies > 0 && theaters > 0) {
                if (sectionIndex == 0) {
                    return PLAYING_HEADER_TYPE;
                } else if (sectionIndex == 1) {
                    return THEATRE_HEADER_TYPE;
                }
            } else if (playingMovies > 0 && cities > 0) {
                if (sectionIndex == 0) {
                    return PLAYING_HEADER_TYPE;
                } else if (sectionIndex == 1) {
                    return CITY_HEADER_TYPE;
                }
            } else if (comingSoonMovies > 0 && theaters > 0) {
                if (sectionIndex == 0) {
                    return COMINGSOON_HEADER_TYPE;
                } else if (sectionIndex == 1) {
                    return THEATRE_HEADER_TYPE;
                }
            } else if (comingSoonMovies > 0 && cities > 0) {
                if (sectionIndex == 0) {
                    return COMINGSOON_HEADER_TYPE;
                } else if (sectionIndex == 1) {
                    return CITY_HEADER_TYPE;
                }
            } else if (theaters > 0 && cities > 0) {
                if (sectionIndex == 0) {
                    return THEATRE_HEADER_TYPE;
                } else if (sectionIndex == 1) {
                    return CITY_HEADER_TYPE;
                }
            }

        } else if (headercount == 1) {
            if (playingMovies > 0) {
                return PLAYING_HEADER_TYPE;
            } else if (comingSoonMovies > 0) {
                return COMINGSOON_HEADER_TYPE;
            } else if (theaters > 0) {
                return THEATRE_HEADER_TYPE;
            } else if (cities > 0) {
                return CITY_HEADER_TYPE;
            }
        }
        return 0;
    }

    @Override
    public int getSectionItemUserType(int sectionIndex, int itemIndex) {
        if (headercount == 4) {
            if (sectionIndex == 0) {
                return PLAYING_HEADER_TYPE;
            } else if (sectionIndex == 1) {
                return COMINGSOON_HEADER_TYPE;
            } else if (sectionIndex == 2) {
                return THEATRE_HEADER_TYPE;
            } else if (sectionIndex == 3) {
                return CITY_HEADER_TYPE;
            }

        } else if (headercount == 3) {
            if (playingMovies > 0 && comingSoonMovies > 0 && theaters > 0) {
                if (sectionIndex == 0) {
                    return PLAYING_HEADER_TYPE;
                } else if (sectionIndex == 1) {
                    return COMINGSOON_HEADER_TYPE;
                } else if (sectionIndex == 2) {
                    return THEATRE_HEADER_TYPE;
                }
            } else if (comingSoonMovies > 0 && theaters > 0 && cities > 0) {
                if (sectionIndex == 0) {
                    return COMINGSOON_HEADER_TYPE;
                } else if (sectionIndex == 1) {
                    return THEATRE_HEADER_TYPE;
                } else if (sectionIndex == 2) {
                    return CITY_HEADER_TYPE;
                }
            } else if (playingMovies > 0 && theaters > 0 && cities > 0) {
                if (sectionIndex == 0) {
                    return PLAYING_HEADER_TYPE;
                } else if (sectionIndex == 1) {
                    return THEATRE_HEADER_TYPE;
                } else if (sectionIndex == 2) {
                    return CITY_HEADER_TYPE;
                }
            } else if (playingMovies > 0 && comingSoonMovies > 0 && cities > 0) {
                if (sectionIndex == 0) {
                    return PLAYING_HEADER_TYPE;
                } else if (sectionIndex == 1) {
                    return COMINGSOON_HEADER_TYPE;
                } else if (sectionIndex == 2) {
                    return CITY_HEADER_TYPE;
                }
            }
        } else if (headercount == 2) {
            if (playingMovies > 0 && comingSoonMovies > 0) {
                if (sectionIndex == 0) {
                    return PLAYING_HEADER_TYPE;
                } else if (sectionIndex == 1) {
                    return COMINGSOON_HEADER_TYPE;
                }
            } else if (playingMovies > 0 && theaters > 0) {
                if (sectionIndex == 0) {
                    return PLAYING_HEADER_TYPE;
                } else if (sectionIndex == 1) {
                    return THEATRE_HEADER_TYPE;
                }
            } else if (playingMovies > 0 && cities > 0) {
                if (sectionIndex == 0) {
                    return PLAYING_HEADER_TYPE;
                } else if (sectionIndex == 1) {
                    return CITY_HEADER_TYPE;
                }
            } else if (comingSoonMovies > 0 && theaters > 0) {
                if (sectionIndex == 0) {
                    return COMINGSOON_HEADER_TYPE;
                } else if (sectionIndex == 1) {
                    return THEATRE_HEADER_TYPE;
                }
            } else if (comingSoonMovies > 0 && cities > 0) {
                if (sectionIndex == 0) {
                    return COMINGSOON_HEADER_TYPE;
                } else if (sectionIndex == 1) {
                    return CITY_HEADER_TYPE;
                }
            } else if (theaters > 0 && cities > 0) {
                if (sectionIndex == 0) {
                    return THEATRE_HEADER_TYPE;
                } else if (sectionIndex == 1) {
                    return CITY_HEADER_TYPE;
                }
            }

        } else if (headercount == 1) {
            if (playingMovies > 0) {
                return PLAYING_HEADER_TYPE;
            } else if (comingSoonMovies > 0) {
                return COMINGSOON_HEADER_TYPE;
            } else if (theaters > 0) {
                return THEATRE_HEADER_TYPE;
            } else if (cities > 0) {
                return CITY_HEADER_TYPE;
            }
        }
        return 0;
    }

    @Override
    public ItemViewHolder onCreateItemViewHolder(ViewGroup parent, int itemType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (itemType) {
            case PLAYING_ITEM_TYPE:
                return new PlayingMovieItemViewHolder(inflater.inflate(R.layout.search_screen_custom, parent, false));

            case COMINGSOON_ITEM_TYPE:
                return new ComingSoonMovieItemViewHolder(inflater.inflate(R.layout.search_screen_custom, parent, false));

            case THEATRE_ITEM_TYPE:
                return new TheatreItemViewHolder(inflater.inflate(R.layout.search_theatre_city_custom_screen, parent, false));

            case CITY_ITEM_TYPE:
                return new CityItemViewHolder(inflater.inflate(R.layout.search_theatre_city_custom_screen, parent, false));
        }

        throw new IllegalArgumentException("Unrecognized itemType: " + itemType);
    }

    @Override
    public HeaderViewHolder onCreateHeaderViewHolder(ViewGroup parent, int headerType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (headerType) {
            case PLAYING_HEADER_TYPE:
                return new HeaderViewHolder(inflater.inflate(R.layout.search_header, parent, false));

            case COMINGSOON_HEADER_TYPE:
                return new HeaderViewHolder(inflater.inflate(R.layout.search_header, parent, false));

            case THEATRE_HEADER_TYPE:
                return new HeaderViewHolder(inflater.inflate(R.layout.search_header, parent, false));

            case CITY_HEADER_TYPE:
                return new HeaderViewHolder(inflater.inflate(R.layout.search_header, parent, false));
        }
        throw new IllegalArgumentException("Unrecognized headerType: " + headerType);
    }

    @Override
    public void onBindItemViewHolder(SectioningAdapter.ItemViewHolder viewHolder, int sectionIndex, final int itemIndex, int itemType) {

        switch (itemType) {
            case PLAYING_ITEM_TYPE: {
                final PlayingMovieItemViewHolder ivh = (PlayingMovieItemViewHolder) viewHolder;

                if (MoviePreferences.isLanguageTurkish(context)) {
                    ivh.movie_name.setText(movieList.PlayingMovies.get(itemIndex).TrName);
                } else {
                    if (movieList.PlayingMovies.get(itemIndex).EnName != null) {
                        ivh.movie_name.setText(movieList.PlayingMovies.get(itemIndex).EnName);
                    } else {
                        ivh.movie_name.setText(movieList.PlayingMovies.get(itemIndex).TrName);
                    }
                }

                Picasso.with(ivh.itemView.getContext()).load(movieList.PlayingMovies.get(itemIndex).TrPosterUrl)
                        .fit().into(ivh.movie_poster, new com.squareup.picasso.Callback() {
                    @Override
                    public void onSuccess() {
                        ivh.movie_progressbar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
                    }
                });

                ivh.search_relativelayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            ActivityOptionsCompat optionsCompat = null;
                            optionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation((Activity) context, ivh.movie_poster, ivh.movie_poster.getTransitionName());
                            Intent intent = new Intent(context, DetailedMovieActivity.class);
                            intent.putExtra("movie_id", movieList.PlayingMovies.get(itemIndex).Id);

                            ivh.movie_poster.buildDrawingCache();
                            Bitmap image = ivh.movie_poster.getDrawingCache();
                            ByteArrayOutputStream bs = new ByteArrayOutputStream();
                            image.compress(Bitmap.CompressFormat.JPEG, 99, bs);
                            intent.putExtra("byteArray", bs.toByteArray());


                            context.startActivity(intent, optionsCompat.toBundle());
                        }
                    }
                });
                break;
            }
            case COMINGSOON_ITEM_TYPE: {
                final ComingSoonMovieItemViewHolder ivh = (ComingSoonMovieItemViewHolder) viewHolder;

                if (MoviePreferences.isLanguageTurkish(context)) {
                    ivh.movie_name.setText(movieList.ComingSoonMovies.get(itemIndex).TrName);
                } else {
                    if (movieList.ComingSoonMovies.get(itemIndex).EnName != null) {
                        ivh.movie_name.setText(movieList.ComingSoonMovies.get(itemIndex).EnName);
                    } else {
                        ivh.movie_name.setText(movieList.ComingSoonMovies.get(itemIndex).TrName);
                    }
                }

                Picasso.with(ivh.itemView.getContext()).load(movieList.ComingSoonMovies.get(itemIndex).TrPosterUrl)
                        .fit().into(ivh.movie_poster, new com.squareup.picasso.Callback() {
                    @Override
                    public void onSuccess() {
                        ivh.movie_progressbar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
                    }
                });

                ivh.search_relativelayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            ActivityOptionsCompat optionsCompat = null;
                            optionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation((Activity) context, ivh.movie_poster, ivh.movie_poster.getTransitionName());
                            Intent intent = new Intent(context, DetailedMovieActivity.class);
                            intent.putExtra("movie_id", movieList.ComingSoonMovies.get(itemIndex).Id);

                            ivh.movie_poster.buildDrawingCache();
                            Bitmap image = ivh.movie_poster.getDrawingCache();
                            ByteArrayOutputStream bs = new ByteArrayOutputStream();
                            image.compress(Bitmap.CompressFormat.JPEG, 99, bs);
                            intent.putExtra("byteArray", bs.toByteArray());
                            context.startActivity(intent, optionsCompat.toBundle());
                        }
                    }
                });

                break;
            }

            case THEATRE_ITEM_TYPE: {
                TheatreItemViewHolder ivh = (TheatreItemViewHolder) viewHolder;
                ivh.theatre_name.setText(movieList.Theaters.get(itemIndex).Name);

                ivh.parent.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, CinemaSessionActivity.class);
                        intent.putExtra("cinema_name", movieList.Theaters.get(itemIndex).Name);
                        intent.putExtra("cinema_id", movieList.Theaters.get(itemIndex).Id);
                        context.startActivity(intent);
                    }
                });

                break;
            }

            case CITY_ITEM_TYPE: {
                CityItemViewHolder ivh = (CityItemViewHolder) viewHolder;
                ivh.city_name.setText(movieList.Cities.get(itemIndex).Name);

                ivh.parent.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                });

                break;
            }

            default:
                throw new IllegalArgumentException("Unrecognized itemType: " + itemType);
        }
    }

    @Override
    public void onBindHeaderViewHolder(SectioningAdapter.HeaderViewHolder viewHolder, int sectionIndex, int headerType) {

        switch (headerType) {
            case PLAYING_HEADER_TYPE: {
                HeaderViewHolder hvh = (HeaderViewHolder) viewHolder;
                hvh.textView.setText(R.string.vizyondakiler);
                break;
            }
            case COMINGSOON_HEADER_TYPE: {
                HeaderViewHolder hvh = (HeaderViewHolder) viewHolder;
                hvh.textView.setText(R.string.coming_soon);
                break;
            }

            case THEATRE_HEADER_TYPE: {
                HeaderViewHolder ivh = (HeaderViewHolder) viewHolder;
                ivh.textView.setText(R.string.salonlar);
                break;
            }

            case CITY_HEADER_TYPE: {
                HeaderViewHolder ivh = (HeaderViewHolder) viewHolder;
                ivh.textView.setText(R.string.province);
                break;
            }

            default:
                throw new IllegalArgumentException("Unrecognized headerType: " + headerType);
        }
    }
}
