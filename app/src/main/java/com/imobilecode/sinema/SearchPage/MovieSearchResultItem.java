package com.imobilecode.sinema.SearchPage;

import com.imobilecode.sinema.Helper.Retrofit.ApiResult;

import java.util.ArrayList;

/**
 * Created by caner on 12-Aug-16.
 */
public class MovieSearchResultItem extends ApiResult {
    public ArrayList<MovieSearchItem> PlayingMovies;
    public ArrayList<MovieSearchItem> ComingSoonMovies;
    public ArrayList<MovieSearchTheatreCityItem> Theaters;
    public ArrayList<MovieSearchTheatreCityItem> Cities;



}
