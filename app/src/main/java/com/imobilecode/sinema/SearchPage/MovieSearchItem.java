package com.imobilecode.sinema.SearchPage;

/**
 * Created by caner on 12-Aug-16.
 */
public class MovieSearchItem {
    public String Id;
    public String TrName;
    public String EnName;
    public String TrPosterUrl;
    public String EnPosterUrl;
    public double ImdbRate;
    public double RottenRate;

}
