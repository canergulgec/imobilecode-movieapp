package com.imobilecode.sinema.SearchPage;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.imobilecode.sinema.Helper.KeyboardHelper;
import com.imobilecode.sinema.Helper.Retrofit.RestManager;
import com.imobilecode.sinema.R;

import org.zakariya.stickyheaders.StickyHeaderLayoutManager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by caner on 10-Aug-16.
 */
public class SearchFragment extends Fragment {
    RecyclerView popular_movie_recyclerview,
            search_movie_recyclerview;
    RelativeLayout popular_relative;
    PopularMovieAdapter adapter;
    SearchMovieAdapter searchresultadapter;
    private SearchView mSearchView;
    private MenuItem searchMenuItem;
    SearchView.OnQueryTextListener listener;
    RestManager mManager;
    Call<MovieSearchResultItem> listCall;
    ProgressBar search_progressbar;
    private static SearchFragment mSearchFragment;


    public static SearchFragment  newInstance(int instance) {
        if (mSearchFragment == null) {
            mSearchFragment = new SearchFragment();
            return mSearchFragment;
        }
        return mSearchFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.search_screen_recyclerview, container, false);

        search_progressbar = (ProgressBar) rootView.findViewById(R.id.search_progress);

        Toolbar toolbar = (Toolbar) rootView.findViewById(R.id.search_toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);

        popular_relative = (RelativeLayout) rootView.findViewById(R.id.popular_movie_relative);

        search_movie_recyclerview = (RecyclerView) rootView.findViewById(R.id.search_movie_recyclerview);
        search_movie_recyclerview.setLayoutManager(new StickyHeaderLayoutManager());
        search_movie_recyclerview.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (RecyclerView.SCROLL_STATE_DRAGGING == newState) {
                    KeyboardHelper.closeKeyboard(getActivity());
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });


        popular_movie_recyclerview = (RecyclerView) rootView.findViewById(R.id.popular_movie_recyclerview);
        adapter = new PopularMovieAdapter(getActivity());
        popular_movie_recyclerview.setLayoutManager(new LinearLayoutManager(getActivity()));
        popular_movie_recyclerview.setAdapter(adapter);

        listener = new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                // newText is text entered by user to SearchView
                if (newText.length() > 2) {
                    popular_relative.setVisibility(View.GONE);
                    search_progressbar.setVisibility(View.VISIBLE);
                    search_movie_recyclerview.setVisibility(View.VISIBLE);

                    filter(newText);
                }else if(newText.length() == 0){
                    popular_relative.setVisibility(View.VISIBLE);
                    search_movie_recyclerview.setVisibility(View.GONE);
                }
                return false;
            }
        };
        return rootView;
    }

    private void filter(String newText) {
        mManager = new RestManager();
        listCall = mManager.getMovieService().getSearchedMovie(newText);
        listCall.enqueue(new Callback<MovieSearchResultItem>() {
            @Override
            public void onResponse(Call<MovieSearchResultItem> call, Response<MovieSearchResultItem> response) {
                if (response.isSuccessful()) {
                    MovieSearchResultItem movieList = response.body();
                    if (movieList != null) {
                        if (movieList.Success) {
                            search_progressbar.setVisibility(View.GONE);
                            searchresultadapter = new SearchMovieAdapter(getActivity(), movieList);
                            search_movie_recyclerview.setAdapter(searchresultadapter);
                        }
                    } else {
                        search_progressbar.setVisibility(View.GONE);
                    }

                }
            }

            @Override
            public void onFailure(Call<MovieSearchResultItem> call, Throwable t) {
            }
        });
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu, menu);
        searchMenuItem = menu.findItem(R.id.action_search);
        mSearchView = (SearchView) searchMenuItem.getActionView();
        LinearLayout ll = (LinearLayout) mSearchView.getChildAt(0);
        LinearLayout ll2 = (LinearLayout) ll.getChildAt(2);
        LinearLayout ll3 = (LinearLayout) ll2.getChildAt(1);
        SearchView.SearchAutoComplete autoComplete = (SearchView.SearchAutoComplete) ll3.getChildAt(0);
        autoComplete.setHint("Film, sinema salonu veya il ara..");
        mSearchView.setOnQueryTextListener(listener);

        MenuItemCompat.setOnActionExpandListener(searchMenuItem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                popular_relative.setVisibility(View.VISIBLE);
                search_movie_recyclerview.setVisibility(View.GONE);
                return true;  // Return true to collapse action view
            }

            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                return true;  // Return true to expand action view
            }
        });
    }
}