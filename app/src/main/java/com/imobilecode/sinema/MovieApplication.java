package com.imobilecode.sinema;

import android.app.Application;
import android.graphics.Movie;
import android.util.Log;

import com.imobilecode.sinema.Helper.MoviePreferences;

/**
 * Created by caner on 02-Aug-16.
 */
public class MovieApplication extends Application {
    private static MovieApplication application;

    public static MovieApplication getApplication() {
        return application;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        application = MovieApplication.this;

    }
}
