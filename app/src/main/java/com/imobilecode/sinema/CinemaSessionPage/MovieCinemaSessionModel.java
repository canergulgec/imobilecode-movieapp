package com.imobilecode.sinema.CinemaSessionPage;

import android.os.Parcel;
import android.os.Parcelable;

import com.imobilecode.sinema.SearchPage.MovieSearchItem;

import java.util.ArrayList;

/**
 * Created by caner on 18-Aug-16.
 */
public class MovieCinemaSessionModel implements Parcelable{
    public MovieSearchItem Movie;
    public ArrayList<MovieCinemaSessionList> SessionList;
    public String Type;

    protected MovieCinemaSessionModel(Parcel in) {
        Type = in.readString();
    }

    public static final Creator<MovieCinemaSessionModel> CREATOR = new Creator<MovieCinemaSessionModel>() {
        @Override
        public MovieCinemaSessionModel createFromParcel(Parcel in) {
            return new MovieCinemaSessionModel(in);
        }

        @Override
        public MovieCinemaSessionModel[] newArray(int size) {
            return new MovieCinemaSessionModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(Type);
    }
}
