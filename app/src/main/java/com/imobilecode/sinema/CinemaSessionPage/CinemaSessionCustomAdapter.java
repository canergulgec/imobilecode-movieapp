package com.imobilecode.sinema.CinemaSessionPage;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.imobilecode.sinema.R;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by caner on 19-Aug-16.
 */
public class CinemaSessionCustomAdapter extends RecyclerView.Adapter<CinemaSessionCustomAdapter.MyViewHolder> {
    Context context;
    String fragmentTitle;
    ArrayList<MovieCinemaSessionModel> mylist;

    public CinemaSessionCustomAdapter(Context context, ArrayList<MovieCinemaSessionModel> mylist, String fragmentTitle) {
        this.context = context;
        this.mylist = mylist;
        this.fragmentTitle = fragmentTitle;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.cinema_session_custom_screen, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        MovieCinemaSessionModel model = mylist.get(position);
        holder.movie_name.setText(model.Movie.TrName);
        holder.movie_type.setText("Salon 3, " + model.Type);
        Picasso.with(holder.itemView.getContext()).load(model.Movie.TrPosterUrl)
                .fit().into(holder.movie_profile, new com.squareup.picasso.Callback() {
            @Override
            public void onSuccess() {
                holder.progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onError() {
            }
        });

        holder.tableRow2.setVisibility(View.GONE);

        holder.tableRow1.removeAllViews();
        holder.tableRow2.removeAllViews();

        int sessionlistsize = model.SessionList.size();

        for (int i = 0; i < sessionlistsize; i++) {
            if (!(i >= 6)) {
                TextView textview = new TextView(context);
                textview.setTextColor(Color.BLACK);
                textview.setText(getTime(model.SessionList.get(i).DateTime));
                holder.tableRow1.addView(textview);
            }
        }

        if (sessionlistsize > 5) {
            holder.tableRow2.setVisibility(View.VISIBLE);
            for (int j = 6; j < sessionlistsize; j++) {
                TextView textview = new TextView(context);
                textview.setTextColor(Color.BLACK);
                textview.setText(getTime(model.SessionList.get(j).DateTime));
                holder.tableRow2.addView(textview);
            }
        }
    }

    private String getTime(String movieCinemaSessionList) {
        return movieCinemaSessionList.substring(0, 5);
    }

    @Override
    public int getItemCount() {
        return mylist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView movie_name, movie_type;
        ImageView movie_profile;
        TableLayout tableLayout;
        TableRow tableRow1, tableRow2;
        ProgressBar progressBar;

        public MyViewHolder(View itemView) {
            super(itemView);
            movie_name = (TextView) itemView.findViewById(R.id.cinema_session_movie_name);
            movie_type = (TextView) itemView.findViewById(R.id.cinema_session_type);
            movie_profile = (ImageView) itemView.findViewById(R.id.cinema_session_profile);
            tableLayout = (TableLayout) itemView.findViewById(R.id.cinema_session_tablelayout);
            tableRow1 = (TableRow) itemView.findViewById(R.id.table_row1);
            tableRow2 = (TableRow) itemView.findViewById(R.id.table_row2);
            progressBar = (ProgressBar) itemView.findViewById(R.id.cinema_session_progressbar);
        }
    }
}
