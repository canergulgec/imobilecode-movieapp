package com.imobilecode.sinema.CinemaSessionPage;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.imobilecode.sinema.Helper.Retrofit.RestManager;
import com.imobilecode.sinema.MapPage.MapActivity;
import com.imobilecode.sinema.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by caner on 18-Aug-16.
 */
public class CinemaSessionActivity extends AppCompatActivity {
    RestManager mManager;
    ViewPager viewPager;
    double cinema_latitude, cinema_longitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cinema_session);

        Toolbar toolbar = (Toolbar) findViewById(R.id.cinema_session_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        String cinema_name = getIntent().getStringExtra("cinema_name");
        TextView toolbar_title = (TextView) findViewById(R.id.cinema_session_toolbar_title);
        toolbar_title.setText(cinema_name);

        cinema_latitude = getIntent().getDoubleExtra("cinema_latitude", 0);
        cinema_longitude = getIntent().getDoubleExtra("cinema_longitude", 0);

        getCinemaSessions();
    }

    private void getCinemaSessions() {
        String cinema_id = getIntent().getStringExtra("cinema_id");
        mManager = new RestManager();
        Call<MovieCinemaSessionResult> listCall = mManager.getMovieService().getCinemaSessions(cinema_id);
        listCall.enqueue(new Callback<MovieCinemaSessionResult>() {
            @Override
            public void onResponse(Call<MovieCinemaSessionResult> call, Response<MovieCinemaSessionResult> response) {
                if (response.isSuccessful()) {
                    MovieCinemaSessionResult detailedCinema = response.body();
                    if (detailedCinema.DateList != null) {
                        for (int i = 0; i < detailedCinema.DateList.size(); i++) {
                            String dayOftheMovie = detailedCinema.DateList.get(i).Date.substring(0, 10);
                            SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
                            Date dt1 = null;
                            try {
                                dt1 = format1.parse(dayOftheMovie);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            DateFormat format2 = new SimpleDateFormat("EE");
                            String finalDay = format2.format(dt1);

                            detailedCinema.DateList.get(i).dayOftheMovies = finalDay;

                        }
                        createViewPager(detailedCinema);
                    }
                }
            }

            @Override
            public void onFailure(Call<MovieCinemaSessionResult> call, Throwable t) {

            }
        });
    }

    private void createViewPager(MovieCinemaSessionResult detailedCinema) {
        viewPager = (ViewPager) findViewById(R.id.cinema_session_viewpager);
        if (viewPager != null) {
            setupViewPager(viewPager, detailedCinema);
        }
        TabLayout tabLayout = (TabLayout) findViewById(R.id.cinema_tabLayout);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setSelectedTabIndicatorColor(Color.WHITE);

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }

    private void setupViewPager(ViewPager viewPager, MovieCinemaSessionResult detailedCinema) {
        CinemaSessionViewPagerAdapter adapter = new CinemaSessionViewPagerAdapter(getSupportFragmentManager());
        for (int i = 0; i < detailedCinema.DateList.size(); i++) {
            adapter.addFrag(new CinemaSessionFragment(), detailedCinema.DateList.get(i).dayOftheMovies,
                    detailedCinema.DateList.get(i).MovieList);
        }
        viewPager.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_map, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == android.R.id.home) {
            finish();
        }
        if (itemId == R.id.action_map) {
            Intent intent = new Intent(getApplicationContext(), MapActivity.class);
            intent.putExtra("cinema_lat", cinema_latitude);
            intent.putExtra("cinema_long", cinema_longitude);
            startActivity(intent);
        }
        return true;
    }
}
