package com.imobilecode.sinema.CinemaSessionPage;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.imobilecode.sinema.CinemaSessionPage.MovieCinemaSessionModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by caner on 19-Aug-16.
 */
public class CinemaSessionViewPagerAdapter extends FragmentPagerAdapter {
    private final List<Fragment> mFragmentList = new ArrayList<>();
    private final List<String> mFragmentTitleList = new ArrayList<>();

    public CinemaSessionViewPagerAdapter(FragmentManager manager) {
        super(manager);
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }

    public void addFrag(Fragment fragment, String title, ArrayList<MovieCinemaSessionModel> movieList) {
        Bundle extras = new Bundle();
        extras.putParcelableArrayList("datelist", movieList);
        extras.putString("fragmentTitle", title);
        fragment.setArguments(extras);

        mFragmentList.add(fragment);
        mFragmentTitleList.add(title);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mFragmentTitleList.get(position);
    }
}