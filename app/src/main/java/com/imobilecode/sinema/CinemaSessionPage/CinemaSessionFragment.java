package com.imobilecode.sinema.CinemaSessionPage;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.imobilecode.sinema.R;

import java.util.ArrayList;

public class CinemaSessionFragment extends Fragment {
    ArrayList<MovieCinemaSessionModel> mylist;
    String fragmentTitle;
    double latitude, longitude;
    RecyclerView cinema_session_recyclerview;
    CinemaSessionCustomAdapter adapter;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.cinema_session_recyclerview, container, false);

        Bundle extras = getArguments();
        if (extras != null) {
            mylist = extras.getParcelableArrayList("datelist");
            fragmentTitle = extras.getString("fragmentTitle");
            latitude = extras.getDouble("cinema_lat");
            longitude = extras.getDouble("cinema_long");
        }
        setHasOptionsMenu(true);

        cinema_session_recyclerview = (RecyclerView) rootView.findViewById(R.id.cinema_session_recyclerview);
        adapter = new CinemaSessionCustomAdapter(getActivity(), mylist, fragmentTitle);
        cinema_session_recyclerview.setLayoutManager(new LinearLayoutManager(getActivity()));
        cinema_session_recyclerview.setAdapter(adapter);

        return rootView;
    }
}

