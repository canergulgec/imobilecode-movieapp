package com.imobilecode.sinema.CinemaSessionPage;

import com.imobilecode.sinema.Helper.Retrofit.ApiResult;

import java.util.ArrayList;

/**
 * Created by caner on 18-Aug-16.
 */
public class MovieCinemaSessionResult extends ApiResult {
    public ArrayList<MovieCinemaSessionItem> DateList;
    public double Latitude;
    public double Longitude;
}
