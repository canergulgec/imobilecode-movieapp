package com.imobilecode.sinema.Helper.Retrofit;


import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by caner on 29-Jul-16.
 */
public class RestManager {

    private MovieService mMovieService;

    public MovieService getMovieService(){
        if(mMovieService == null){
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(ServiceUrls.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(OkHttpProvider.getInstance())
                    .build();

            mMovieService = retrofit.create(MovieService.class);
        }
        return mMovieService;
    }
}
