package com.imobilecode.sinema.Helper.Retrofit;

import com.imobilecode.sinema.CinemaSessionPage.MovieCinemaSessionResult;
import com.imobilecode.sinema.DetailMoviePage.MovieDetailResultItem;
import com.imobilecode.sinema.CinemaPage.MovieCinemasResultItem;
import com.imobilecode.sinema.SettingsPage.InfoItem;
import com.imobilecode.sinema.MoviePage.MovieResultItem;
import com.imobilecode.sinema.SearchPage.MovieSearchResultItem;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;

/**
 * Created by caner on 29-Jul-16.
 */
public interface MovieService {

    @GET(ServiceUrls.MOVIE_LIST + "/type" + "/{movietype}")
    Call<MovieResultItem> getAllMovies(@Path("movietype")int movietype);

    @GET(ServiceUrls.MOVIE_LIST + "/{movieid}")
    Call<MovieDetailResultItem> getDetailedMovie(@Path("movieid")String movieid);

    @GET(ServiceUrls.MOVIE_SEARCH + "/{movie_letter}")
    Call<MovieSearchResultItem> getSearchedMovie(@Path("movie_letter")String movie_letter);

    @Headers("DeviceId: c247ad96-806f-4a44-a0a4-e5ff48c007f8")
    @GET(ServiceUrls.MOVIE_CINEMA)
    Call<MovieCinemasResultItem> getCinemas();

    @GET(ServiceUrls.MOVIE_CINEMA + "/{cinema_id}")
    Call<MovieCinemaSessionResult> getCinemaSessions(@Path("cinema_id")String cinema_id);

    //TODO service id düzelt
    @GET(ServiceUrls.SETTINGS_URL + "34")
    Call<InfoItem> getApplications();

}
