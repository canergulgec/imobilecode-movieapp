package com.imobilecode.sinema.Helper;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by caner on 07-Aug-16.
 */
public class MoviePreferences {
    private static final String PREFERENCES_FILE = "EventApp";
    private static final String FIRST_TIME_OPENED = "detailed_open";
    private static final String VIEWHOLDER2_FIRST_OPENED = "viewholder2_open";
    private static final String LANGUAGE = "language";


    public static void setDetailActivityActive(Context context, boolean flag){
        SharedPreferences preferences = context.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        editor.putBoolean(FIRST_TIME_OPENED, flag);
        editor.apply();
    }

    public static boolean isDetailActivityActive(Context context){
        SharedPreferences preferences = context.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);
        return preferences.getBoolean(FIRST_TIME_OPENED, false);
    }

    public static void setViewholder2FirstOpened(Context context, boolean flag){
        SharedPreferences preferences = context.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        editor.putBoolean(VIEWHOLDER2_FIRST_OPENED, flag);
        editor.apply();
    }

    public static boolean isViewHolder2Opened(Context context){
        SharedPreferences preferences = context.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);
        return preferences.getBoolean(VIEWHOLDER2_FIRST_OPENED, false);
    }


    public static void setLanguageTurkish(Context context, boolean flag){
        SharedPreferences preferences = context.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        editor.putBoolean(LANGUAGE, flag);
        editor.apply();
    }

    public static boolean isLanguageTurkish(Context context){
        SharedPreferences preferences = context.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);
        return preferences.getBoolean(LANGUAGE, false);
    }

}
