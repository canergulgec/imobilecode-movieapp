package com.imobilecode.sinema.Helper.Retrofit;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;

public class OkHttpProvider {

    private static OkHttpClient sOkHttpClient;

    private OkHttpProvider(){}

    public static OkHttpClient getInstance(){
        if(sOkHttpClient == null){
            HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
            httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

            OkHttpClient.Builder builder= new OkHttpClient.Builder()
                    .addNetworkInterceptor(httpLoggingInterceptor)
                    .connectTimeout(30, TimeUnit.SECONDS)
                    .readTimeout(30, TimeUnit.SECONDS)
                    .writeTimeout(30, TimeUnit.SECONDS);

            sOkHttpClient = builder.build();
        }
        return sOkHttpClient;
    }
}

