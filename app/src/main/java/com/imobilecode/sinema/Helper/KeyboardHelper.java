package com.imobilecode.sinema.Helper;

import android.app.Activity;
import android.content.Context;
import android.view.inputmethod.InputMethodManager;

/**
 * Created by caner on 01-Sep-16.
 */
public class KeyboardHelper {

    public static void closeKeyboard(Activity activity) {
        if (activity != null && activity.getCurrentFocus() != null) {
            activity.getCurrentFocus().clearFocus();

            InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);

            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), inputMethodManager.HIDE_NOT_ALWAYS);
        }

    }
}
