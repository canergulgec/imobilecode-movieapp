package com.imobilecode.sinema.CustomTextView;

import android.graphics.Typeface;
import android.support.v4.util.LruCache;

import com.imobilecode.sinema.MovieApplication;

public class TypefaceCacheHelper {
    private static TypefaceCacheHelper mTypefaceCache;

    private final LruCache<String, Typeface> mTypefaceHashMap;

    protected TypefaceCacheHelper() {
        mTypefaceHashMap = new LruCache<String, Typeface>(5);
    }

    public static TypefaceCacheHelper getInstance() {
        if (mTypefaceCache == null) {
            mTypefaceCache = new TypefaceCacheHelper();
        }

        return mTypefaceCache;
    }

    public Typeface getTypeface(FontEnum fontEnum) {
        synchronized (mTypefaceHashMap) {
            String fileName = getFontFileName(fontEnum.ordinal());
            Typeface typeface = mTypefaceHashMap.get(fileName);
            if (typeface == null) {
                typeface = Typeface.createFromAsset(MovieApplication.getApplication().getAssets(), fileName);
                mTypefaceHashMap.put(fileName, typeface);
            }

            return typeface;
        }
    }

    private String getFontFileName(int fontStyle) {
        String fileName = null;
        switch (fontStyle) {
            case 0:// Regular
                fileName = "fonts/Roboto-Regular.ttf";
                break;
            case 1:// Medium
                fileName = "fonts/Roboto-Medium.ttf";
                break;
            case 2:// Light
                fileName = "fonts/Roboto-Light.ttf";
                break;
            case 3://Bold
                fileName = "fonts/Roboto-Bold.ttf";
                break;
            default:
                break;
        }

        return fileName;
    }
}
