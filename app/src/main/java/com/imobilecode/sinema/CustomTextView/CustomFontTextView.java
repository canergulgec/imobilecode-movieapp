package com.imobilecode.sinema.CustomTextView;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Paint;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

import com.imobilecode.sinema.R;

public class CustomFontTextView extends AppCompatTextView {
    public CustomFontTextView(Context context) {
        this(context, null, 0);
    }

    public CustomFontTextView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CustomFontTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        if (!isInEditMode()) {
            init(context, attrs, defStyle);
        }
    }

    private void init(Context context, AttributeSet attrs, int defStyle) {
        setPaintFlags(this.getPaintFlags() | Paint.SUBPIXEL_TEXT_FLAG);
        int fontStyle = 0;
        if (attrs != null) {
            TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.CustomFontTextView, defStyle, 0);
            fontStyle = ta.getInt(R.styleable.CustomFontTextView_fontStyle, fontStyle);
            ta.recycle();
        }

        setFontStyle(fontStyle);
    }

    private void setFontStyle(int fontStyle) {
        this.setTypeface(TypefaceCacheHelper.getInstance().getTypeface(FontEnum.values()[fontStyle]));
    }
}

