package com.imobilecode.sinema.CustomTextView;

/**
 * Created by caner on 02-Aug-16.
 */
public enum FontEnum {
    ROBOTO_REGULAR, ROBOTO_MEDIUM, ROBOTO_LIGHT, ROBOTO_BOLD
}
