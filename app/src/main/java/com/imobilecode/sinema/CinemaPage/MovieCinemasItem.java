package com.imobilecode.sinema.CinemaPage;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by caner on 17-Aug-16.
 */
public class MovieCinemasItem implements Parcelable {
    public String Id;
    public String Name;
    public double Latitude;
    public double Longitude;

    public MovieCinemasItem(){

    }

    protected MovieCinemasItem(Parcel in) {
        Id = in.readString();
        Name = in.readString();
        Latitude = in.readDouble();
        Longitude = in.readDouble();
    }

    public static final Creator<MovieCinemasItem> CREATOR = new Creator<MovieCinemasItem>() {
        @Override
        public MovieCinemasItem createFromParcel(Parcel in) {
            return new MovieCinemasItem(in);
        }

        @Override
        public MovieCinemasItem[] newArray(int size) {
            return new MovieCinemasItem[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(Id);
        dest.writeString(Name);
        dest.writeDouble(Latitude);
        dest.writeDouble(Longitude);
    }
}
