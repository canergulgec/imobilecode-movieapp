package com.imobilecode.sinema.CinemaPage;

import com.imobilecode.sinema.Helper.Retrofit.ApiResult;
import com.imobilecode.sinema.SearchPage.MovieCitiesItem;

import java.util.ArrayList;

/**
 * Created by caner on 17-Aug-16.
 */
public class MovieCinemasResultItem extends ApiResult {
    public ArrayList<MovieCinemasItem> NearCinemas;
    public ArrayList<MovieCitiesItem> Cities;

}
