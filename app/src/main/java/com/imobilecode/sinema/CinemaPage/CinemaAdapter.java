package com.imobilecode.sinema.CinemaPage;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.imobilecode.sinema.CinemaSessionPage.CinemaSessionActivity;
import com.imobilecode.sinema.R;

import org.zakariya.stickyheaders.SectioningAdapter;

/**
 * Created by caner on 16-Aug-16.
 */

/// LAYOUTLARA BAK DEĞİŞTİR ONLARI!!!!!!!
public class CinemaAdapter extends SectioningAdapter {
    static final int NEAR_CINEMA_HEADER_TYPE = 0;
    static final int PROVINCE_HEADER_TYPE = 1;
    static final int NEAR_CINEMA_ITEM_TYPE = 0;
    static final int PROVINCE_ITEM_TYPE = 1;
    Context context;
    MovieCinemasResultItem movieList;

    public CinemaAdapter(Context context, MovieCinemasResultItem movieList) {
        this.context = context;
        this.movieList = movieList;
    }

    public class NearCinemasItemHolder extends SectioningAdapter.ItemViewHolder {
        TextView near_movie_text;
        View parent;

        public NearCinemasItemHolder(View itemView) {
            super(itemView);
            parent = itemView;
            near_movie_text = (TextView) itemView.findViewById(R.id.search_theatre_text);
        }
    }

    public class NearCinemasFooter extends SectioningAdapter.FooterViewHolder {
        TextView textView;
        View parent;

        public NearCinemasFooter(View itemView) {
            super(itemView);
            parent = itemView;
            textView = (TextView) itemView.findViewById(R.id.cinema_footer_text);


        }
    }

    public class ProvincesItemHolder extends SectioningAdapter.ItemViewHolder {
        TextView province_text;
        View parent;

        public ProvincesItemHolder(View itemView) {
            super(itemView);
            parent = itemView;
            province_text = (TextView) itemView.findViewById(R.id.search_theatre_text);
        }
    }

    public class HeaderViewHolder extends SectioningAdapter.HeaderViewHolder {
        TextView textView;

        public HeaderViewHolder(View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.theatre_header_text);
        }
    }

    @Override
    public int getNumberOfSections() {
        return 2;
    }

    @Override
    public int getNumberOfItemsInSection(int sectionIndex) {
        if (sectionIndex == 0) {
            return 4;
        } else {
            return movieList.Cities.size();
        }
    }

    @Override
    public boolean doesSectionHaveHeader(int sectionIndex) {
        return true;
    }

    @Override
    public boolean doesSectionHaveFooter(int sectionIndex) {
        if (sectionIndex == 0) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int getSectionHeaderUserType(int sectionIndex) {
        if (sectionIndex == 0) {
            return NEAR_CINEMA_HEADER_TYPE;
        } else {
            return PROVINCE_HEADER_TYPE;
        }
    }

    @Override
    public int getSectionItemUserType(int sectionIndex, int itemIndex) {
        if (sectionIndex == 0) {
            return NEAR_CINEMA_ITEM_TYPE;
        } else {
            return PROVINCE_ITEM_TYPE;
        }
    }

    @Override
    public int getSectionFooterUserType(int sectionIndex) {
        return 1;
    }


    @Override
    public ItemViewHolder onCreateItemViewHolder(ViewGroup parent, int itemType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (itemType) {
            case NEAR_CINEMA_ITEM_TYPE:
                return new NearCinemasItemHolder(inflater.inflate(R.layout.search_theatre_city_custom_screen, parent, false));

            case PROVINCE_ITEM_TYPE:
                return new ProvincesItemHolder(inflater.inflate(R.layout.search_theatre_city_custom_screen, parent, false));

        }

        throw new IllegalArgumentException("Unrecognized itemType: " + itemType);
    }

    @Override
    public FooterViewHolder onCreateFooterViewHolder(ViewGroup parent, int footerType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        return new NearCinemasFooter(inflater.inflate(R.layout.cinema_footer, parent, false));


    }


    @Override
    public HeaderViewHolder onCreateHeaderViewHolder(ViewGroup parent, int headerType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (headerType) {
            case NEAR_CINEMA_HEADER_TYPE:
                return new HeaderViewHolder(inflater.inflate(R.layout.search_header, parent, false));

            case PROVINCE_HEADER_TYPE:
                return new HeaderViewHolder(inflater.inflate(R.layout.search_header, parent, false));

        }

        throw new IllegalArgumentException("Unrecognized headerType: " + headerType);
    }

    @Override
    public void onBindItemViewHolder(SectioningAdapter.ItemViewHolder viewHolder, int sectionIndex, final int itemIndex, int itemType) {
        switch (itemType) {
            case NEAR_CINEMA_ITEM_TYPE: {
                final NearCinemasItemHolder ivh = (NearCinemasItemHolder) viewHolder;
                ivh.near_movie_text.setText(movieList.NearCinemas.get(itemIndex).Name);
                ivh.parent.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, CinemaSessionActivity.class);
                        intent.putExtra("cinema_name", movieList.NearCinemas.get(itemIndex).Name);
                        intent.putExtra("cinema_id", movieList.NearCinemas.get(itemIndex).Id);
                        intent.putExtra("cinema_latitude", movieList.NearCinemas.get(itemIndex).Latitude);
                        intent.putExtra("cinema_longitude", movieList.NearCinemas.get(itemIndex).Longitude);
                        context.startActivity(intent);

                    }
                });

                break;
            }
            case PROVINCE_ITEM_TYPE: {
                ProvincesItemHolder ivh = (ProvincesItemHolder) viewHolder;
                ivh.province_text.setText(movieList.Cities.get(itemIndex).Name);
                ivh.parent.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                });

                break;
            }

            default:
                throw new IllegalArgumentException("Unrecognized itemType: " + itemType);
        }
    }

    @Override
    public void onBindHeaderViewHolder(SectioningAdapter.HeaderViewHolder viewHolder, int sectionIndex, int headerType) {
        switch (headerType) {
            case NEAR_CINEMA_HEADER_TYPE: {
                HeaderViewHolder hvh = (HeaderViewHolder) viewHolder;
                hvh.textView.setText("BANA EN YAKIN SİNEMALAR");
                break;
            }
            case PROVINCE_HEADER_TYPE: {
                HeaderViewHolder hvh = (HeaderViewHolder) viewHolder;
                hvh.textView.setText("İLLER");
                break;
            }

            default:
                throw new IllegalArgumentException("Unrecognized headerType: " + headerType);
        }
    }

    @Override
    public void onBindFooterViewHolder(SectioningAdapter.FooterViewHolder viewHolder, int sectionIndex, int footerType) {
        NearCinemasFooter fvh = (NearCinemasFooter) viewHolder;
        fvh.textView.setText("Daha fazla göster");

        fvh.parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, NearCinemasActivity.class);
                intent.putParcelableArrayListExtra("near_cinema_list", movieList.NearCinemas);
                context.startActivity(intent);
            }
        });
    }
}
