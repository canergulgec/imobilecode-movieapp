package com.imobilecode.sinema.CinemaPage;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.imobilecode.sinema.CinemaSessionPage.CinemaSessionActivity;
import com.imobilecode.sinema.R;

import java.util.ArrayList;

/**
 * Created by caner on 17-Aug-16.
 */
public class NearCinemasAdapter extends RecyclerView.Adapter<NearCinemasAdapter.MyViewHolder> {
    Context context;
    ArrayList<MovieCinemasItem> getItems;

    public NearCinemasAdapter(Context context, ArrayList<MovieCinemasItem> getItems){
        this.context = context;
        this.getItems = getItems;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.near_movie_custom_screen,parent,false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        MovieCinemasItem current = getItems.get(position);
        holder.near_cinema_text.setText(current.Name);

        holder.parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, CinemaSessionActivity.class);
                intent.putExtra("cinema_name",getItems.get(position).Name);
                intent.putExtra("cinema_id",getItems.get(position).Id);
                intent.putExtra("cinema_latitude",getItems.get(position).Latitude);
                intent.putExtra("cinema_longitude",getItems.get(position).Longitude);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return getItems.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        TextView near_cinema_text;
        View parent;
        public MyViewHolder(View itemView) {
            super(itemView);
            parent = itemView;
            near_cinema_text = (TextView) itemView.findViewById(R.id.near_cinema_name_text);
        }
    }

}
