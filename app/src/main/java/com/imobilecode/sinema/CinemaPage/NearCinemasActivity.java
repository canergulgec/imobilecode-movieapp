package com.imobilecode.sinema.CinemaPage;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.imobilecode.sinema.CinemaPage.NearCinemasAdapter;
import com.imobilecode.sinema.CinemaPage.MovieCinemasItem;
import com.imobilecode.sinema.R;

import java.util.ArrayList;

/**
 * Created by caner on 17-Aug-16.
 */
public class NearCinemasActivity extends AppCompatActivity {
    RecyclerView near_movies_recyclerview;
    NearCinemasAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.near_cinema_screen);

        near_movies_recyclerview = (RecyclerView) findViewById(R.id.near_cinema_recyclerview);

        Toolbar toolbar = (Toolbar) findViewById(R.id.near_cinemas_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        ArrayList<MovieCinemasItem> myList =  getIntent().getParcelableArrayListExtra(("near_cinema_list"));
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        near_movies_recyclerview.setLayoutManager(mLayoutManager);
        adapter = new NearCinemasAdapter(this,myList);
        near_movies_recyclerview.setAdapter(adapter);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if(itemId == android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
