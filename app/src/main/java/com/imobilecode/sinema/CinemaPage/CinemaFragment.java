package com.imobilecode.sinema.CinemaPage;


import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.imobilecode.sinema.Helper.Retrofit.RestManager;
import com.imobilecode.sinema.R;

import org.zakariya.stickyheaders.StickyHeaderLayoutManager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by caner on 16-Aug-16.
 */
public class CinemaFragment extends Fragment{
    RecyclerView cinema_recyclerview;
    CinemaAdapter adapter;
    RestManager mManager;
    Call<MovieCinemasResultItem> listCall;
    private static CinemaFragment mCinemaFragment;
    ProgressBar cinema_progressbar;

    public static CinemaFragment newInstance(int instance) {

        if (mCinemaFragment == null) {
            mCinemaFragment = new CinemaFragment();
            return mCinemaFragment;
        }
        return mCinemaFragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.cinema_layout, container, false);

        cinema_progressbar = (ProgressBar) rootView.findViewById(R.id.cinema_progressbar);
        Toolbar toolbar = (Toolbar) rootView.findViewById(R.id.cinema_toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);

        cinema_recyclerview = (RecyclerView) rootView.findViewById(R.id.cinema_recyclerview);
        cinema_recyclerview.setLayoutManager(new StickyHeaderLayoutManager());

        getCinemas();

        return rootView;
    }

    private void getCinemas() {
        mManager = new RestManager();
        listCall = mManager.getMovieService().getCinemas();
        listCall.enqueue(new Callback<MovieCinemasResultItem>() {
            @Override
            public void onResponse(Call<MovieCinemasResultItem> call, Response<MovieCinemasResultItem> response) {
                if (response.isSuccessful()) {
                    cinema_progressbar.setVisibility(View.GONE);
                    MovieCinemasResultItem movieList = response.body();
                    adapter = new CinemaAdapter(getActivity(), movieList);
                    cinema_recyclerview.setAdapter(adapter);
                }
            }

            @Override
            public void onFailure(Call<MovieCinemasResultItem> call, Throwable t) {

            }
        });
    }

    @Override
    public void onDestroy() {
        listCall.cancel();
        super.onDestroy();
    }
}

