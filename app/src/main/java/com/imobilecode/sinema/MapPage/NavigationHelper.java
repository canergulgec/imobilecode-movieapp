package com.imobilecode.sinema.MapPage;

/**
 * Created by caner on 26-Aug-16.
 */

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;

import com.imobilecode.sinema.R;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

public class NavigationHelper {
    public enum NavigationAppEnum {
        YandexNavigator, YandexMaps, GoogleMaps
    }

    private String[] navigationAppPackageNameArray = new String[]{
            "ru.yandex.yandexmaps", "ru.yandex.yandexnavi",
            "com.google.android.apps.maps"};

    private ArrayList<NavigationHelperItem> mNavigationHelperItems;
    private WeakReference<Context> mWeakReferenceContext;


    public NavigationHelper(Context context) {
        mWeakReferenceContext = new WeakReference<Context>(context);
        mNavigationHelperItems = new ArrayList<NavigationHelperItem>();
    }

    public void openNavigation(double latitude, double longitude) {

        Context context = mWeakReferenceContext.get();

        if (context != null) {
            for (int i = 0; i < navigationAppPackageNameArray.length; i++) {
                NavigationHelperItem item = getNavigationItemIfAppExist(
                        navigationAppPackageNameArray[i], latitude, longitude);

                if (item != null) {
                    mNavigationHelperItems.add(item);
                }
            }

            if (mNavigationHelperItems.size() == 1) {
                context.startActivity(mNavigationHelperItems.get(0).getIntent());
            } else {
                showNavigationDialog();
            }
        }
    }

    private void showNavigationDialog() {
        final Context context = mWeakReferenceContext.get();

        if (context != null) {
            String[] titleItems = new String[mNavigationHelperItems.size()];

            for (int i = 0; i < mNavigationHelperItems.size(); i++) {
                titleItems[i] = mNavigationHelperItems.get(i).getTitle();
            }

            showAlertDialog(titleItems, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    try {
                        NavigationHelperItem mHelperItem = mNavigationHelperItems.get(which);

                        context.startActivity(mHelperItem.getIntent());
                    } catch (Exception e) {

                    }
                }
            });
        }
    }

    private void showAlertDialog(String[] items,
                                 DialogInterface.OnClickListener mClickListener) {
        Context context = mWeakReferenceContext.get();
        if (context != null && !((Activity) context).isFinishing()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.MyAlertDialogStyle);

            builder.setTitle(context.getString(R.string.map_dialog_open_navigate));
            builder.setItems(items, mClickListener);

            AlertDialog dialog = builder.create();
            dialog.show();
        }
    }

    private NavigationHelperItem getNavigationItemIfAppExist(
            String packageName, double latitude, double longitude) {

        NavigationHelperItem item = null;

        if (isAppInstall(packageName)) {
            if ("ru.yandex.yandexmaps".equals(packageName)) {

                Intent intent = new Intent("ru.yandex.yandexmaps.action.BUILD_ROUTE_ON_MAP");
                intent.setPackage("ru.yandex.yandexmaps");
                intent.putExtra("lat_to", latitude);
                intent.putExtra("lon_to", longitude);
                intent.putExtra("show_jams", 1);

                item = new NavigationHelperItem("Yandex.Maps", intent, NavigationAppEnum.YandexMaps);

            } else if ("ru.yandex.yandexnavi".equals(packageName)) {

                Intent intent = new Intent("ru.yandex.yandexnavi.action.BUILD_ROUTE_ON_MAP");
                intent.setPackage("ru.yandex.yandexnavi");
                intent.putExtra("lat_to", latitude);
                intent.putExtra("lon_to", longitude);
                intent.putExtra("show_jams", 1);

                item = new NavigationHelperItem("Yandex.Navigator", intent, NavigationAppEnum.YandexNavigator);
            } else if ("com.google.android.apps.maps".equals(packageName)) {

                Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                        Uri.parse("http://maps.google.com/maps?daddr="
                                + latitude + "," + longitude));
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addCategory(Intent.CATEGORY_LAUNCHER);
                intent.setClassName("com.google.android.apps.maps",
                        "com.google.android.maps.MapsActivity");

                item = new NavigationHelperItem("Google Maps", intent, NavigationAppEnum.GoogleMaps);
            }
        }
        return item;
    }

    private boolean isAppInstall(String pakageName) {
        Context context = mWeakReferenceContext.get();

        boolean isAppInstalled = false;

        if (context != null) {
            PackageManager pm = context.getPackageManager();
            try {
                pm.getPackageInfo(pakageName, PackageManager.GET_ACTIVITIES);
                isAppInstalled = true;
            } catch (NameNotFoundException e) {
                isAppInstalled = false;
            }
        }

        return isAppInstalled;
    }
}

