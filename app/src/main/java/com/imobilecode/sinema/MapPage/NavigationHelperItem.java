package com.imobilecode.sinema.MapPage;

import android.content.Intent;

import com.imobilecode.sinema.MapPage.NavigationHelper;

/**
 * Created by caner on 26-Aug-16.
 */
public class NavigationHelperItem
{
    private String title;
    private Intent intent;
    private NavigationHelper.NavigationAppEnum navigationAppEnum;

    public NavigationHelperItem(String title, Intent intent, NavigationHelper.NavigationAppEnum navigationAppEnum)
    {
        this.intent = intent;
        this.title = title;
        this.navigationAppEnum = navigationAppEnum;
    }

    public Intent getIntent()
    {
        return this.intent;
    }

    public String getTitle()
    {
        return title;
    }

    public NavigationHelper.NavigationAppEnum getNavigationAppEnum()
    {
        return navigationAppEnum;
    }
}
